﻿using UnityEngine;
using System.Collections;

public class QuitGame : MonoBehaviour {

    void OnTriggerStay(Collider what)
    {
        // If player is in trigger zone and pushes up and is not shooting and is not moving to either side
        if ((what.gameObject.tag == "Player" || what.gameObject.tag == "HeadCollider") && Input.GetAxisRaw("Vertical") > 0 && !Input.GetButton("Fire1") && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 0f)
        {
            Application.Quit();
        }
    }
}
