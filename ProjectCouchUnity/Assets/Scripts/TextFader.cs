﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextFader : MonoBehaviour {

    public float minAlpha = 0.153f;
    Text textComponent;
    float zoneWidth;

    void Start()
    {
        textComponent = GetComponent<Text>();
        textComponent.color = new Vector4(textComponent.color.r, textComponent.color.g, textComponent.color.b, minAlpha);
        zoneWidth = GetComponent<BoxCollider>().size.x;
    }

    void OnTriggerStay(Collider what)
    {
        
        if (what.tag == "Player") {
            float percent = 0;

            // if right is in the x or -x direction
            if (Mathf.Abs(transform.right.x) > Mathf.Abs(transform.right.y) && Mathf.Abs(transform.right.x) > Mathf.Abs(transform.right.z))
            {
                if ((what.transform.position.x) > (transform.position.x))
                {
                    percent = Mathf.Abs((what.transform.position.x) - (transform.position.x)) / (zoneWidth / 3);
                }
                else
                {
                    percent = Mathf.Abs((transform.position.x) - (what.transform.position.x)) / (zoneWidth / 3);
                }
            }
            // if right is in the y or -y direction
            else if (Mathf.Abs(transform.right.y) > Mathf.Abs(transform.right.x) && Mathf.Abs(transform.right.y) > Mathf.Abs(transform.right.z))
            {
                if ((what.transform.position.y) > (transform.position.y))
                {
                    percent = Mathf.Abs((what.transform.position.y) - (transform.position.y)) / (zoneWidth / 3);
                }
                else
                {
                    percent = Mathf.Abs((transform.position.y) - (what.transform.position.y)) / (zoneWidth / 3);
                }
            }

            // if right is in the z or -z direction
            else if (Mathf.Abs(transform.right.z) > Mathf.Abs(transform.right.x) && Mathf.Abs(transform.right.z) > Mathf.Abs(transform.right.y))
            {
                if ((what.transform.position.z) > (transform.position.z))
                {
                    percent = Mathf.Abs((what.transform.position.z) - (transform.position.z)) / (zoneWidth / 3);
                }
                else
                {
                    percent = Mathf.Abs((transform.position.z) - (what.transform.position.z)) / (zoneWidth / 3);
                }
            }
            else
            {
                Debug.LogError("No right way?");
            }

            float percentDistanceFromCenter = 1 - percent;
            //Debug.Log(percentDistanceFromCenter);
            if (percentDistanceFromCenter >= minAlpha)
            {
                textComponent.color = new Vector4(textComponent.color.r, textComponent.color.g, textComponent.color.b, percentDistanceFromCenter);
            }
            
        }
        
    }


    void OnTriggerExit(Collider what)
    {
        if (what.tag == "Player")
        {
            textComponent.color = new Vector4(textComponent.color.r, textComponent.color.g, textComponent.color.b, minAlpha);
        }
    }
}
