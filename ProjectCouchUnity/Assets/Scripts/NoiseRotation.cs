﻿using UnityEngine;
using System.Collections;

public class NoiseRotation : MonoBehaviour {

    // Use this for initialization
    void Start()
    {
        StartCoroutine(NoiseRotate());
    }

    IEnumerator NoiseRotate()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.0f, 0.3f));
            transform.eulerAngles = new Vector3(transform.eulerAngles.x + Random.Range(-0.15f, 0.15f), transform.eulerAngles.y + Random.Range(-0.15f, 0.15f), transform.eulerAngles.z + Random.Range(-0.15f, 0.15f));
        }
    }
}
