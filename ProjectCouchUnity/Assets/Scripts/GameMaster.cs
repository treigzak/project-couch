﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMaster : MonoBehaviour {

    [Header("Cube Scaling")]
    public float cubeScaleWhenSmall = 1.5f;
    public float cubeScaleWhenPlayed = 15f;
    public float cubeScaleWhenBig = 150f;
    public float scaleSpeed = 1f;
    [Space(10f)]
    public GameObject startingLevel;
    public GameObject[] levelPrefabs;
    public AudioClip travelSound;

    [HideInInspector]
    public int currentLevel = 0;
    private Controller playerController;
    [HideInInspector]
    public List<GameObject> spawnedLevels = new List<GameObject>();


    public AudioClip preGameSound;

    private AudioSource preGameSoundSource;


    //private Vector3 worldScale;
    //private Vector3 startScale;
    private Vector3 cubeScaleWhenSmallVec;
    private Vector3 cubeScaleWhenPlayedVec;
    private Vector3 cubeScaleWhenBigVec;

    private float startTime;
    private float lastStep = 0f;

	private bool transitionBlocked;
    private bool forceChange = false;

    private AudioSource effectPlayer;
    private bool scaleUp = false;
    private bool scaleDown = false;

    private LightHandler lightHandler;


    public bool inPreGameState = true;


    private bool playingPreGameSound = false;


	// Use this for initialization
	void Start () {
        effectPlayer = GameObject.FindGameObjectWithTag("VariousSoundfx").GetComponent<AudioSource>();

		transitionBlocked = false;
        cubeScaleWhenSmallVec = new Vector3(cubeScaleWhenSmall, cubeScaleWhenSmall, cubeScaleWhenSmall);
        cubeScaleWhenPlayedVec = new Vector3(cubeScaleWhenPlayed, cubeScaleWhenPlayed, cubeScaleWhenPlayed);
        cubeScaleWhenBigVec = new Vector3(cubeScaleWhenBig, cubeScaleWhenBig, cubeScaleWhenBig);

        //startScale = startingLevel.transform.localScale;

        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<Controller>();
        Transform spawnPoint = null;
        GoOutOfCube goOutOfCube;

        foreach (Transform child in startingLevel.transform.FindChild("OutOfCubeConnections"))
        {
            if (child.transform.name == "OutOfCube") {
                goOutOfCube = child.GetComponent<GoOutOfCube>();
                if (goOutOfCube.thisId == 0) {
                    spawnPoint = child;
                    break;
                }
            }
        }

        lightHandler = GetComponent<LightHandler>();

        if (spawnPoint == null) {
            Debug.LogError("Shit! Could not find spawnpoint");
        }

        playerController.SetCurrentLevel(startingLevel, spawnPoint);
        playerController.CalculateAndSetCubeRotation(false);

        spawnedLevels.Add(startingLevel);

        //worldScale = startingLevel.transform.localScale;

        if (currentLevel == levelPrefabs.Length - 1)
        {
            Debug.Log("There is no next level defined");
            return;
        }
        
		addNextPrefabLevel ();


        preGameSoundSource = GameObject.FindGameObjectWithTag("Narrator").GetComponent<AudioSource>();
        // Horribel å høre på all mikrofonblåsinga, så commenta ut Oo
        //preGameSoundSource.PlayOneShot(preGameSound);



	}

    void Update()
    {



        if (inPreGameState)
        {


            if (!playingPreGameSound)
            {
                playingPreGameSound = true;
                Debug.Log("Starting presound");

                preGameSoundSource.clip = preGameSound;
                //preGameSoundSource.loop = false;
                preGameSoundSource.Play();


            }
            else if (!preGameSoundSource.isPlaying)
            {

                Debug.Log("presound DONE");
                inPreGameState = false;

            }


            return;

        }


        if (scaleUp)
        {
            // Scale up played cube from played to big
            if (currentLevel - 1 >= 0 && currentLevel - 1 < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel - 1].transform.localScale != cubeScaleWhenBigVec)
                {
                    // !! Various ways to lerp, might be handy to keep around !!
                    //
                    // Very fast out, very ease in, will take ages to reach (startingLevel.transform.localScale == worldScale)
                    //startingLevel.transform.localScale = Vector3.Lerp(startingLevel.transform.localScale, worldScale, Time.deltaTime * scaleSpeed);

                    // Linear, looks like shit without easing in, but startingLevel.transform.localScale == worldScale will happen fast.
                    //float scaleAmnt = (Time.time - startTime) * scaleSpeed;
                    //startingLevel.transform.localScale = Vector3.Lerp(startScale, worldScale, scaleAmnt);

                    // Ease in
                    //float t = (Time.time - startTime) * scaleSpeed;
                    //t = Mathf.Sin(t * Mathf.PI * 0.5f);
                    //if (lastStep > t) {
                    //    t = 1f;
                    //}
                    //lastStep = t;
                    //startingLevel.transform.localScale = Vector3.Lerp(startScale, worldScale, t);

                    // Smoothstep ease in/out
                    //float t = (Time.time - startTime) * scaleSpeed;
                    //t = t * t * (3f - 2f * t);
                    //if (lastStep > t) {
                    //    t = 1f;
                    //}
                    //lastStep = t;
                    //startingLevel.transform.localScale = Vector3.Lerp(startScale, worldScale, t);


                    // Smoothstep ease in/out more
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel - 1].transform.localScale = Vector3.Lerp(cubeScaleWhenPlayedVec, cubeScaleWhenBigVec, t);

                }

            }

            // Scale up small inner cube from small to played
            if (currentLevel >= 0 && currentLevel < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel].transform.localScale != cubeScaleWhenPlayedVec)
                {
                    // Smoothstep ease in/out more
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel].transform.localScale = Vector3.Lerp(cubeScaleWhenSmallVec, cubeScaleWhenPlayedVec, t);
                }
                else if (transitionBlocked)
                {
                    transitionBlocked = false;
                    scaleUp = false;

                    // Activate all rigid bodies of current level
                    foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("RigidBodies"))
                    {
                        child.rigidbody.isKinematic = false;

                        if (child.tag != "Enemy")
                        {
                            
                            child.rigidbody.useGravity = true;
                        }
                        
                        // If it is an enemy, enable them
                        if (child.tag == "Enemy")
                        {
                            child.GetComponent<EnemyStopToShootController>().SetEnabled(true);
                        }
                    }

                    playerController.controllerAble = true;

                }
            }

            // Scale up new small inner cube from nothing to small
            if (currentLevel + 1 >= 0 && currentLevel + 1 < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel + 1].transform.localScale != cubeScaleWhenBigVec)
                {
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel + 1].transform.localScale = Vector3.Lerp(Vector3.zero, cubeScaleWhenSmallVec, t);
                }
            }
        }

        if (scaleDown)
        {
            // Scale down big cube from big to played
            if (currentLevel >= 0 && currentLevel < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel].transform.localScale != cubeScaleWhenPlayedVec)
                {

                    // Smoothstep ease in/out more
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel].transform.localScale = Vector3.Lerp(cubeScaleWhenBigVec, cubeScaleWhenPlayedVec, t);

                }
                else if (transitionBlocked)
                {
                    transitionBlocked = false;
                    scaleDown = false;

                    // Activate all rigid bodies of current level
                    foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("RigidBodies"))
                    {
                        child.rigidbody.isKinematic = false;


                        if (child.tag != "Enemy")
                        {
                            
                            child.rigidbody.useGravity = true;
                        }



                        // If it is an enemy, enable them
                        if (child.tag == "Enemy")
                        {
                            child.GetComponent<EnemyStopToShootController>().SetEnabled(true);
                        }
                    }

                    playerController.controllerAble = true;

                }
            }

            // Scale down played from played to small
            if (currentLevel + 1 >= 0 && currentLevel + 1 < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel + 1].transform.localScale != Vector3.zero)
                {
                    // Smoothstep ease in/out more
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel + 1].transform.localScale = Vector3.Lerp(cubeScaleWhenPlayedVec, cubeScaleWhenSmallVec, t);
                }
            }

            // Scale down small cube from small to zero
            if (currentLevel + 2 >= 0 && currentLevel + 2 < spawnedLevels.Count)
            {
                if (spawnedLevels[currentLevel + 2].transform.localScale != Vector3.zero)
                {
                    float t = (Time.time - startTime) * scaleSpeed;
                    t = t * t * t * (t * (6f * t - 15f) + 10f);
                    if (lastStep > t)
                    {
                        t = 1f;
                    }
                    lastStep = t;

                    spawnedLevels[currentLevel + 2].transform.localScale = Vector3.Lerp(cubeScaleWhenSmallVec, Vector3.zero, t);
                }
                
            }
        }


        // Skipping levels
        // Going to next level
        if (Input.GetKeyDown(KeyCode.E))
        {
            Debug.Log("Skipping level");


            // Checking IntoCubeConnections for next level before OutOfCube
            foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("IntoCubeConnections"))
            {
                if (child.tag == "NextLevel")
                {
                    forceChange = true;
                    GoInwardOneLevel(child.GetComponent<GoIntoCube>().connectedToId);

                    return;
                }
            }

            // If no exit was found in IntoCubeConnections, check OutOfCube
            foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("OutOfCubeConnections"))
            {
                if (child.tag == "NextLevel")
                {
                    forceChange = true;
                    GoOutwardOneLevel(child.GetComponent<GoOutOfCube>().connectedToId);

                    return;
                }
            }
        }

        // Going to previous level
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Debug.Log("Going back a level");


            // Checking OutOfCube for next level before IntoCubeConnections
            foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("OutOfCubeConnections"))
            {
                if (child.tag == "PreviousLevel")
                {
                    forceChange = true;
                    GoOutwardOneLevel(child.GetComponent<GoOutOfCube>().connectedToId);

                    return;
                }
            }

            // Checking IntoCubeConnections for next level before OutOfCube
            foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("IntoCubeConnections"))
            {
                if (child.tag == "PreviousLevel")
                {
                    forceChange = true;
                    GoInwardOneLevel(child.GetComponent<GoIntoCube>().connectedToId);

                    return;
                }
            }
        }


    }


	private void addNextPrefabLevel(){

		GameObject nextLevel = (GameObject)Instantiate(levelPrefabs[currentLevel + 1], startingLevel.transform.position, startingLevel.transform.rotation);
		//nextLevel.transform.parent = spawnedLevels[currentLevel].transform.FindChild("NextLevelSpawnPoint");
		nextLevel.transform.localScale = cubeScaleWhenSmallVec;
		spawnedLevels.Add(nextLevel);

        // Deactivate all rigid bodies of next level
        foreach (Transform child in nextLevel.transform.FindChild("RigidBodies").transform)
        {

            child.rigidbody.isKinematic = true;


            if (child.tag != "Enemy")
            {
                
                child.rigidbody.useGravity = false;
            }

            


            // If it is an enemy, disable them
            if (child.tag == "Enemy")
            {
                child.GetComponent<EnemyStopToShootController>().SetEnabled(false);
            }

                // Its a box
            //else
            //{
            //    child.GetComponent<MeshRenderer>().castShadows = false;
            //    child.GetComponent<MeshRenderer>().receiveShadows = false;
            //}
        }

	}
	

    public void GoOutwardOneLevel(int exitId){
        

        if (!forceChange)
        {
            if (transitionBlocked)
            {
                return;
            }

            // If player is not grounded, cannot enter door
            if (!playerController.GetGrounded())
            {
                return;
            }
        }
        forceChange = false;

        Debug.Log("Go out");
        
        if (currentLevel == 0) {
            Debug.Log("Cannot go to -1 level");
            return;
        }
        // Deactivate all rigid bodies of current level before going to next level
        foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("RigidBodies"))
        {

            child.rigidbody.isKinematic = true;

            if (child.tag != "Enemy")
            {
                
                child.rigidbody.useGravity = false;
            }

            // If it is an enemy, disable them
            if (child.tag == "Enemy")
            {
                child.GetComponent<EnemyStopToShootController>().SetEnabled(false);
            }
        }

        //startScale = startingLevel.transform.localScale;
        //worldScale /= 10f;
        startTime = Time.time;

        currentLevel--;

		transitionBlocked = true;

        
        // Find the GoIntoCube trigger which is paired with this GoOutOfCube
        Transform spawnPoint = null;
        GoIntoCube goIntoCube;

        foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("IntoCubeConnections"))
        {
            if (child.transform.name == "IntoCube")
            {
                goIntoCube = child.GetComponent<GoIntoCube>();
                if (goIntoCube.thisId == exitId)
                {
                    spawnPoint = child;
                    break;
                }
            }
        }

        if (spawnPoint == null)
        {
            Debug.LogError("Shit! Could not find spawnpoint");
        }

        playerController.SetCurrentLevel(spawnedLevels[currentLevel], spawnPoint);

        spawnedLevels[currentLevel].GetComponent<BoxCollider>().enabled = true;
        //spawnedLevels[currentLevel].GetComponent<MeshRenderer>().castShadows = true;
        //spawnedLevels[currentLevel].GetComponent<MeshRenderer>().receiveShadows = true;
        lastStep = 0f;

		playerController.controllerAble = false;

        effectPlayer.PlayOneShot(travelSound);
        scaleDown = true;
    }

    public void GoInwardOneLevel(int exitId)
    {
        if (!forceChange)
        {
            if (transitionBlocked)
            {
                return;
            }

            // If player is not grounded, cannot enter door
            if (!playerController.GetGrounded())
            {
                return;
            }
        }
        forceChange = false;

        Debug.Log("Go in");

        if (currentLevel == 3)
        {
            lightHandler.gameOn = true;

        }
        

        if (currentLevel == levelPrefabs.Length - 1)
        {
            Debug.Log("There is no next level defined");
            return;
        }

        spawnedLevels[currentLevel].GetComponent<BoxCollider>().enabled = false;
        //spawnedLevels[currentLevel].GetComponent<MeshRenderer>().castShadows = false;
        //spawnedLevels[currentLevel].GetComponent<MeshRenderer>().receiveShadows = false;
        
        // Deactivate all rigid bodies of current level before going to next level
        foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("RigidBodies")) {

            child.rigidbody.isKinematic = true;

            if (child.tag != "Enemy")
            {
                child.rigidbody.useGravity = false;
            }

            // If it is an enemy, disable them
            if (child.tag == "Enemy")
            {
                child.GetComponent<EnemyStopToShootController>().SetEnabled(false);
            }

                // Its a box
            //else
            //{
            //    child.GetComponent<MeshRenderer>().castShadows = false;
            //    child.GetComponent<MeshRenderer>().receiveShadows = false;
            //}
        }
             

        //startScale = startingLevel.transform.localScale;
        //worldScale *= 10f;
        startTime = Time.time;

        currentLevel++;

        

        Transform spawnPoint = null;
        GoOutOfCube goOutOfCube;

        foreach (Transform child in spawnedLevels[currentLevel].transform.FindChild("OutOfCubeConnections"))
        {
            if (child.transform.name == "OutOfCube")
            {
                goOutOfCube = child.GetComponent<GoOutOfCube>();
                if (goOutOfCube.thisId == exitId)
                {
                    spawnPoint = child;
                    break;
                }
            }
        }

        if (spawnPoint == null)
        {
            Debug.LogError("Shit! Could not find spawnpoint");
        }

        playerController.SetCurrentLevel(spawnedLevels[currentLevel], spawnPoint);

        transitionBlocked = true;
        lastStep = 0f;

        playerController.controllerAble = false;

        effectPlayer.PlayOneShot(travelSound);
        scaleUp = true;

        if (currentLevel == levelPrefabs.Length - 1)
        {
            Debug.Log("There is no inner cube defined to instantiate");
            return;
        }

        if (spawnedLevels.Count - 2 < currentLevel) {
            addNextPrefabLevel();
        }

        
    }
}
