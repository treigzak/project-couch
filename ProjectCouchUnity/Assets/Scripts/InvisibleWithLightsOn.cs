﻿using UnityEngine;
using System.Collections;

public class InvisibleWithLightsOn : MonoBehaviour {

    private MeshRenderer graphics;
    private Light light;

	// Use this for initialization
	void Start () {
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        graphics = transform.FindChild("Character Graphics").GetComponent<MeshRenderer>();
        graphics.enabled = !light.enabled;
	}
	
	// Update is called once per frame
	void Update () {
        if (graphics.enabled == light.enabled)
        {
            graphics.enabled = !graphics.enabled;
        }
        
	}
}
