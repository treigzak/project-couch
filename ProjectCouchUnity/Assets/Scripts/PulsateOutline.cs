﻿using UnityEngine;
using System.Collections;

public class PulsateOutline : MonoBehaviour {

    private float outline;
    private bool expand = true;
    private float min = 0.002f;
    private float max = 0.004f;
    private float changePerSec = 0.004f;

	// Use this for initialization
	void Start () {
        outline = renderer.material.GetFloat("_Outline");

	}

    void Update()
    {
        outline += expand ? changePerSec * Time.deltaTime : -changePerSec * Time.deltaTime;
        if (outline >= max)
        {
            outline = max;
            expand = !expand;
        }
        else if (outline <= min)
        {
            outline = min;
            expand = !expand;
        }

        renderer.material.SetFloat("_Outline", outline);
    }

}
