﻿using UnityEngine;
using System.Collections;

public class GrabLedge : MonoBehaviour {

    CapsuleCollider playerCapsuleCollider;
    Transform playerTransform;
    Controller playerController;
    Rigidbody playerRB;
    Transform graphicsTransform;
    //bool grabbing = false;
    float grabCooldown = 1.5f;
    float lastGrab = 0f;

	// Use this for initialization
	void Start () {
        playerTransform = transform.parent.parent.transform;
        playerCapsuleCollider = transform.parent.parent.GetComponent<CapsuleCollider>();
        playerController = transform.parent.parent.GetComponent<Controller>();
        playerRB = transform.parent.parent.GetComponent<Rigidbody>();
        graphicsTransform = transform.parent.transform;
	}

    void Update()
    {
        //if (grabbing)
        //{
        //    if (Input.GetButtonDown("Jump"))
        //    {
        //        grabbing = false;
        //        playerController.controllerAble = true;
        //        playerRB.useGravity = true;
        //        playerRB.isKinematic = false;
        //        playerController.Jump();
        //    }
        //}
    }
	

    void OnTriggerEnter(Collider other)
    {
        if (playerController.grabbing)
        {
            return;
        }
        if (other.tag == "Grabbable")
        {
            
            // Cannot grab until x seconds has passed since you grabbed last time
            if (Time.time < lastGrab + grabCooldown)
            {
                return;
            }
            // If they both are pointing upwards in relatively the same direction
            if (Mathf.Abs(other.transform.up.x - transform.up.x) < 0.05 && Mathf.Abs(other.transform.up.y - transform.up.y) < 0.05 && Mathf.Abs(other.transform.up.z - transform.up.z) < 0.05)
            {
                // If they both are pointing right in relatively the same direction, which means the player is moving towards the ledge
                if (Mathf.Abs(other.transform.right.x - transform.right.x) < 0.05 && Mathf.Abs(other.transform.right.y - transform.right.y) < 0.05 && Mathf.Abs(other.transform.right.z - transform.right.z) < 0.05)
                {
                    playerController.controllerAble = false;
                    playerRB.velocity = Vector3.zero;
                    playerRB.useGravity = false;
                    playerRB.isKinematic = true;
                    playerTransform.position = other.transform.position + playerController.GetCurrentUpVector() * -0.4f; //-x to have him hang lower
                    graphicsTransform.rotation = other.transform.rotation;
                    playerController.jumpsDone = 0;

                    playerController.grabbing = true;

                    //Test to use capsule collider to state how far left/right to pos the player 
                    playerTransform.position += other.transform.right * -playerCapsuleCollider.radius;
                    //Physics.IgnoreCollision(transform.parent.parent.collider, other);
                    //Physics.IgnoreCollision(transform.parent.parent.FindChild("HeadCollider").collider, other);

                    lastGrab = Time.time;
                    return;
                }
                
            }
            // or they are pointing in opposite directions
            else if (Mathf.Abs(other.transform.up.x + transform.up.x) < 0.05 && Mathf.Abs(other.transform.up.y + transform.up.y) < 0.05 && Mathf.Abs(other.transform.up.z + transform.up.z) < 0.05)
            {
                // If they both are pointing right in the same direction, which means the player is moving towards the ledge
                if (Mathf.Abs(other.transform.right.x - transform.right.x) < 0.05 && Mathf.Abs(other.transform.right.y - transform.right.y) < 0.05 && Mathf.Abs(other.transform.right.z - transform.right.z) < 0.05)
                {
                    playerController.controllerAble = false;
                    playerRB.velocity = Vector3.zero;
                    playerRB.useGravity = false;
                    playerRB.isKinematic = true;
                    playerTransform.position = other.transform.position + playerController.GetCurrentUpVector() * -0.4f; //-x to have him hang lower
                    graphicsTransform.rotation = other.transform.rotation;
                    graphicsTransform.Rotate(new Vector3(180, 0, 0)); // Rotate 180 degrees in x to have him face the correct way
                    playerController.jumpsDone = 0;

                    //Test to use capsule collider to state how far left/right to pos the player 
                    playerTransform.position += other.transform.right * -playerCapsuleCollider.radius;
                    //Physics.IgnoreCollision(transform.parent.parent.collider, other);
                    //Physics.IgnoreCollision(transform.parent.parent.FindChild("HeadCollider").collider, other);

                    playerController.grabbing = true;

                    lastGrab = Time.time;
                    return;
                }
                
            }

            //Debug.Log("Grabss");
            //Debug.Log("Up: " + other.transform.up);
            //Debug.Log("Up: " + transform.up);

            //Debug.Log("Right: " + other.transform.right);
            //Debug.Log("Right: " + transform.right);
        }
        
    }
}
