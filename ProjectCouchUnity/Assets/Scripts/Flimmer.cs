﻿using UnityEngine;
using System.Collections;

public class Flimmer : MonoBehaviour {

    public int matIndex = 0;
    public Vector2 animRate = new Vector2(1.0f, 0.0f);
    public string textureName = "_MainTex";
    private Color color;

    Vector2 offset = Vector2.zero;

    void Start()
    {
        color = renderer.material.GetColor("_Color");
        StartCoroutine(Flick());
    }

	
	void LateUpdate () {
        offset += (animRate * Time.deltaTime);

        if (renderer.enabled) {
            renderer.materials[matIndex].SetTextureOffset(textureName, offset);
        }
	}

    IEnumerator Flick()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.0f, 0.1f));
            color.a = Random.Range(0.0f, 0.1f);
            renderer.material.SetColor("_Color", color);
        }
    }
}
