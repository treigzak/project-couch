﻿using UnityEngine;

[AddComponentMenu("Character/Controller")]
[RequireComponent(typeof(CapsuleCollider))]
//[RequireComponent(typeof(MouseLook))]
//[RequireComponent(typeof(Croucher))]
[RequireComponent(typeof(Rigidbody))]

/*

 * Public functions

 * Footstep();
 * IsGrounded();
 * IsMoving();
 * CanStand();
 * Jump();
 * int getUpDir();
 * float getTurnPoint()

*/
public class Controller : MonoBehaviour
{

    enum Direction { x, y, z, negx, negy, negz };

    public GameObject currentLevel;
    public GameObject characterGraphics;
    [Header("Cube rotation")]
    //public Transform nextLevelCube;
    public Transform everythingRotating;
    public float rotationSpeed = 1f;
    public float maxRotation = 22.5f;  // Should be maximum 45
    [Space(10f)]
    public bool controllerAble = true;
    //public Transform camera;    //The root object that contains the camera
    public float checksPerSecond = 10f; //The amount of times per second to run IsGrounded(), CanStand() and IsMoving()

    [Header("Falling")]
    public float gravity = -9.8f;
    public float fallThreshold = 3f;    //How many units to fall to start counting as damage
    public float fallDamageMultiplier = 3f;
    public float maxFallSpeed = 20f;

    //Croucher croucher;
    CapsuleCollider capsule;

    [Header("Footsteps")]
    public AudioSource audioSource;
    public AudioClip footstepSound;
    public float footstepSpeed = 1.5f;

    [Header("Speed")]
    public float currentSpeed;
    public float maxHorizontalSpeed = 10;
    [Space(10f)]
    public float walkSpeed = 5f;
    public float crouchSpeed = 2.5f;
    public float climbSpeed = 5.5f;
    public float runSpeed = 7.5f;

    [Header("Running")]
    public bool ableToRun = false;
    public int stamina = 100;
    public float staminaRegenPerSecond = 15f;
    float nextStamina;
    [HideInInspector]
    public int maxStamina = 100;

    [Header("Jumping")]
    public bool canJump = true;
    public float jumpHeight = 8f;   //in unity units
    public int maxJumps = 2;
    public float airAccelerator = 0.5f;
    public float groundAccelerator = 1.5f;
    public float groundAcceleratorInDark = 0.5f;
    public float maxJumpValue = 100;
    public float increaseRate = 1f;
    private float jumpValue = 0f;
    private bool canJumpHigher = false;

    //Private variables
    bool grounded;
    float nextCheck;
    bool canStand;
    float speed;
    float acceleration;
    float jumpedYPos;
    float landedYPos;
    bool lastGrounded;
    bool moving;
    bool lastCrouching;
    float nextFootstep;
    Rigidbody myRB;
    float lastAiredPos;
    Vector3 velocityChange;
    Direction updir = Direction.y;
    Direction rightdir = Direction.x;
    float turnPoint = 0.526f;
    bool canSwap = true;
    bool rotateCamera = false;
    Vector3 cameraTarget;
    Transform feetPosition;
    bool jumpDown = false;
    Transform headCollider;
    ApeAnimHandler animHandler;
    Transform spawnPoint;
    Vector3 spawnCameraRotation;
    float respawnCooldown = 0;
    Direction spawnUpDir = Direction.y;
    Direction spawnRightDir = Direction.x;
    Vector3 spawnGravity;




    private Light light;



    //Hidden variables
    [HideInInspector]
    public bool crouching;
    //This can be used to alter the speed from another script
    [HideInInspector]
    public float speedMultiplier = 1f;
    [HideInInspector]
    public int jumpsDone = 0;
    [HideInInspector]
    public bool grabbing = false;

    void Start()
    {
        Physics.gravity = new Vector3(0, gravity, 0);
        spawnGravity = Physics.gravity;

        feetPosition = transform.FindChild("FeetPosition");

        cameraTarget = everythingRotating.rotation.eulerAngles;
        headCollider = transform.FindChild("HeadCollider");
        animHandler = characterGraphics.transform.FindChild("Anim").GetComponent<ApeAnimHandler>();

        //croucher = GetComponent<Croucher>();
        capsule = GetComponent<CapsuleCollider>();

        myRB = GetComponent<Rigidbody>();
        myRB.freezeRotation = true;

        maxStamina = stamina;

        grounded = IsGrounded();
        lastGrounded = grounded;


        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();


        //If this is networked, make sure that the rigidbody is kinematic is true for the
        //people with a !networkView.isMine (myRB.isKinematic = true;)
    }

    public Vector3 GetJumpVerticalVelocity()
    {
        Vector3 tempvec = Vector3.zero;
        switch (updir)
        {
            case Direction.y:
                tempvec.x = myRB.velocity.x;
                tempvec.y = CalculateJumpVerticalSpeed();
                tempvec.z = myRB.velocity.z;
                break;

            case Direction.z:
                tempvec.x = myRB.velocity.x;
                tempvec.y = myRB.velocity.y;
                tempvec.z = CalculateJumpVerticalSpeed();
                break;

            case Direction.x:
                tempvec.x = CalculateJumpVerticalSpeed();
                tempvec.y = myRB.velocity.y;
                tempvec.z = myRB.velocity.z;
                break;

            case Direction.negy:
                tempvec.x = myRB.velocity.x;
                tempvec.y = -CalculateJumpVerticalSpeed();
                tempvec.z = myRB.velocity.z;
                break;

            case Direction.negz:
                tempvec.x = myRB.velocity.x;
                tempvec.y = myRB.velocity.y;
                tempvec.z = -CalculateJumpVerticalSpeed();
                break;

            case Direction.negx:
                tempvec.x = -CalculateJumpVerticalSpeed();
                tempvec.y = myRB.velocity.y;
                tempvec.z = myRB.velocity.z;
                break;

            default:
                Debug.Log("Unknown direction");
                break;
        }

        return tempvec;
    }


    public void ReduceSideVelocity(int reduceDividant)
    {
        Vector3 tempvec = Vector3.zero;

        switch (updir)
        {
            case Direction.y:
                tempvec.x = myRB.velocity.x - myRB.velocity.x / reduceDividant;
                tempvec.y = myRB.velocity.y;
                tempvec.z = myRB.velocity.z - myRB.velocity.z / reduceDividant;
                break;

            case Direction.z:
                tempvec.x = myRB.velocity.x - myRB.velocity.x / reduceDividant;
                tempvec.y = myRB.velocity.y - myRB.velocity.y / reduceDividant;
                tempvec.z = myRB.velocity.z;
                break;

            case Direction.x:
                tempvec.x = myRB.velocity.x;
                tempvec.y = myRB.velocity.y - myRB.velocity.y / reduceDividant;
                tempvec.z = myRB.velocity.z - myRB.velocity.z / reduceDividant;
                break;


            case Direction.negy:
                tempvec.x = myRB.velocity.x - myRB.velocity.x / reduceDividant;
                tempvec.y = myRB.velocity.y;
                tempvec.z = myRB.velocity.z - myRB.velocity.z / reduceDividant;
                break;

            case Direction.negz:
                tempvec.x = myRB.velocity.x - myRB.velocity.x / reduceDividant;
                tempvec.y = myRB.velocity.y - myRB.velocity.y / reduceDividant;
                tempvec.z = myRB.velocity.z;
                break;

            case Direction.negx:
                tempvec.x = myRB.velocity.x;
                tempvec.y = myRB.velocity.y - myRB.velocity.y / reduceDividant;
                tempvec.z = myRB.velocity.z - myRB.velocity.z / reduceDividant;
                break;

            default:
                Debug.Log("Unknown direction");
                break;
        }

        myRB.velocity = tempvec;
    }

    public void JumpDown()
    {
        if (!canJump)
        {
            return;
        }
        if (grounded)
        {
            jumpDown = true;
            //myRB.velocity += GetJumpVerticalVelocity();
            myRB.velocity += GetCurrentUpVector() * 4;
            animHandler.PlayJumpAnim();
        }
    }

    public void Jump()
    {
        if (!canJump)
        {
            return;
        }
        if (grounded)
        {
            //Normal jumping if the player can stand
            if (canStand)
            {
                jumpValue = jumpHeight;
                canJumpHigher = true;
                jumpsDone = 0;
                lastAiredPos = transform.position.y;
                animHandler.PlayJumpAnim();

                myRB.velocity = GetJumpVerticalVelocity();
            }
        }
        else
        {
            //Double jumping in mid air if possible
            if (jumpsDone < maxJumps - 1)
            {
                jumpValue = jumpHeight;
                canJumpHigher = true;
                jumpsDone++;
                lastAiredPos = transform.position.y;

                animHandler.PlayJumpAnim();

                if (Input.GetAxisRaw("Horizontal") != 0)
                {
                    Vector3 tempvec = Vector3.zero;

                    switch (updir)
                    {
                        case Direction.y:
                            switch (rightdir)
                            {
                                case Direction.x:
                                    tempvec.x = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.y = CalculateJumpVerticalSpeed();
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.negx:
                                    tempvec.x = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.y = CalculateJumpVerticalSpeed();
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.z:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = CalculateJumpVerticalSpeed();
                                    tempvec.z = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.negz:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = CalculateJumpVerticalSpeed();
                                    tempvec.z = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;


                        case Direction.z:
                            switch (rightdir)
                            {
                                case Direction.x:
                                    tempvec.x = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.negx:
                                    tempvec.x = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.y:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.z = CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.negy:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.z = CalculateJumpVerticalSpeed();
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;

                        case Direction.x:
                            switch (rightdir)
                            {
                                case Direction.z:
                                    tempvec.x = CalculateJumpVerticalSpeed();
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.negz:
                                    tempvec.x = CalculateJumpVerticalSpeed();
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.y:
                                    tempvec.x = CalculateJumpVerticalSpeed();
                                    tempvec.y = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.negy:
                                    tempvec.x = CalculateJumpVerticalSpeed();
                                    tempvec.y = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;

                        case Direction.negy:
                            switch (rightdir)
                            {
                                case Direction.x:
                                    tempvec.x = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.y = -CalculateJumpVerticalSpeed();
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.negx:
                                    tempvec.x = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.y = -CalculateJumpVerticalSpeed();
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.z:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = -CalculateJumpVerticalSpeed();
                                    tempvec.z = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.negz:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = -CalculateJumpVerticalSpeed();
                                    tempvec.z = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;


                        case Direction.negz:
                            switch (rightdir)
                            {
                                case Direction.x:
                                    tempvec.x = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = -CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.negx:
                                    tempvec.x = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = -CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.y:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.z = -CalculateJumpVerticalSpeed();
                                    break;

                                case Direction.negy:
                                    tempvec.x = myRB.velocity.x;
                                    tempvec.y = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.z = -CalculateJumpVerticalSpeed();
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;

                        case Direction.negx:
                            switch (rightdir)
                            {
                                case Direction.z:
                                    tempvec.x = -CalculateJumpVerticalSpeed();
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.negz:
                                    tempvec.x = -CalculateJumpVerticalSpeed();
                                    tempvec.y = myRB.velocity.y;
                                    tempvec.z = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    break;

                                case Direction.y:
                                    tempvec.x = -CalculateJumpVerticalSpeed();
                                    tempvec.y = walkSpeed * Input.GetAxisRaw("Horizontal");
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                case Direction.negy:
                                    tempvec.x = -CalculateJumpVerticalSpeed();
                                    tempvec.y = walkSpeed * -Input.GetAxisRaw("Horizontal");
                                    tempvec.z = myRB.velocity.z;
                                    break;

                                default:
                                    Debug.Log("Unknown direction");
                                    break;
                            }
                            break;

                        default:
                            Debug.Log("Unknown direction");
                            break;
                    }

                    myRB.velocity = tempvec;
                }
                else
                {
                    myRB.velocity = GetJumpVerticalVelocity();
                }

            }
        }
    }
    void Update()
    {
        // Gravity
        //Vector3 worldRotation = new Vector3(cubeWorld.transform.rotation.x, cubeWorld.transform.rotation.y, cubeWorld.transform.rotation.z);
        //myRB.velocity = new Vector3(myRB.velocity.x + gravity * Time.deltaTime, myRB.velocity.y + gravity * Time.deltaTime , myRB.velocity.z);

        //if (rotateCamera) {
        //    everythingRotating.rotation = Quaternion.Lerp(everythingRotating.rotation, Quaternion.Euler(cameraTarget), 3 * Time.deltaTime);
        //    if (everythingRotating.rotation == Quaternion.Euler(cameraTarget))
        //    {
        //        rotateCamera = false;
        //    }
        //}
        // Camera rotation based on player position
        CalculateAndSetCubeRotation(true);



        // If the player can swap sides, rotate player when he is close enough to the edge
        if (canSwap && transform.parent.tag != "MovingPlatform")
        {
            switch (updir)
            {
                // ======== If up direction is y ========
                case Direction.y:
                    switch (rightdir)
                    {
                        // ======== If right direction is x ========
                        case Direction.x:
                            // If going in negative x (left)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(0, 90, 0);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in x (right)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in y (up)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.z;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative y (down)
                            else if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, -oldGravity.y);
                                    updir = Direction.negz;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = -90;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is z ========
                        case Direction.z:
                            // If going in negative z (left)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in z (right)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint); ;
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in y (up)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negx;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative y (down)
                            else if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.x;

                                    cameraTarget.x -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -x ========
                        case Direction.negx:
                            // If going in negative negx, which is positive x velocity (left)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in negative x (right)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in y (up)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(90f, 180f, 0f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, -oldGravity.y);
                                    updir = Direction.negz;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = 90;
                                    cameraTarget.y = 180;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative y (down)
                            else if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(270, 180, 0);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.z;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 280;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -z ========
                        case Direction.negz:
                            // If going in negative negz, which is positive z velocity (left)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in negative z (right)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in y (up)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.x;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 90;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 0;

                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative y (down)
                            else if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negx;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = 270;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown direction");
                            break;
                    }
                    break;


                // ======== If up direction is z ========
                case Direction.z:
                    switch (rightdir)
                    {
                        // ======== If right direction is x ========
                        case Direction.x:
                            // If going in negative x (left)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in x (right)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y -= 90;
                                    //cameraTarget.z -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 270;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in z (up)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, -oldGravity.z, oldGravity.y);
                                    updir = Direction.negy;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative y (down)
                            else if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(0, 0, 0);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.y;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is y ========
                        case Direction.y:
                            // If going in negative y (left)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;

                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in y (right)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in z (up)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.x;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative z (down)
                            else if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negx;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -x ========
                        case Direction.negx:
                            // If going in negative negx, which is positive x velocity (left)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 270f);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    //cameraTarget.x += 90;
                                    //cameraTarget.y += 90;
                                    //cameraTarget.z += 270;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 270;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in negative x (right)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in z (up)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.y;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 180;
                                    cameraTarget.z = 0;

                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative z (down)
                            else if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, -oldGravity.z, oldGravity.y);
                                    updir = Direction.negy;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -y ========
                        case Direction.negy:
                            // If going in negative negy, which is positive y velocity (left)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }


                            // If going in negative y (right)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;

                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in z (up)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negx;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative z (down)
                            else if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.x;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown direction");
                            break;
                    }
                    break;


                // ======== If up direction is x ========
                case Direction.x:
                    switch (rightdir)
                    {
                        // ======== If right direction is z ========
                        case Direction.z:
                            // If going in negative z (left)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.z - 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in z (right)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z + 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint); ;
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in x (up)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.y;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, -oldGravity.x, oldGravity.y);
                                    updir = Direction.negy;

                                    //cameraTarget.x += 90;
                                    //cameraTarget.y -= 180;
                                    //cameraTarget.z -= 180;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is y ========
                        case Direction.y:
                            // If going in negative y (left)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;

                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in y (right)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in x (up)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, -oldGravity.x);
                                    updir = Direction.x;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.z;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -z ========
                        case Direction.negz:
                            // If going in negative negz, which is positive z velocity (left)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in negative z (right)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y -= 90;
                                    //cameraTarget.z -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in x (up)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 180, transform.rotation.eulerAngles.z - 180);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, -oldGravity.x, oldGravity.z);
                                    updir = Direction.negy;

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 180;
                                    cameraTarget.z -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.y;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -y ========
                        case Direction.negy:
                            // If going in negative negy, which is positive y velocity (left)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }


                            // If going in negative y (right)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;

                                    rightdir = Direction.z;
                                }
                            }

                            // If going in x (up)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.z;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, -oldGravity.x);
                                    updir = Direction.negz;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown right direction");
                            break;
                    }
                    break;

                // ======== If up direction is -y ========
                case Direction.negy:
                    switch (rightdir)
                    {
                        // ======== If right direction is x ========
                        case Direction.x:
                            // If going in negative x (left)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.x - 270f, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(0f, 90f, 180f);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in x (right)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in -y (up)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 180, transform.rotation.eulerAngles.z + 180);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.negz;

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 180;
                                    cameraTarget.z += 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -y (down)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, -oldGravity.y);
                                    updir = Direction.z;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 90;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is z ========
                        case Direction.z:
                            // If going in negative z (left)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in z (right)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint); ;
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in -y (up)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y + 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.x;

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 180;
                                    cameraTarget.z -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -y (down)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negx;

                                    cameraTarget.x += 90;
                                    cameraTarget.x += 180;
                                    cameraTarget.x -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -x ========
                        case Direction.negx:
                            // If going in negative negx, which is positive x velocity (left)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in negative x (right)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in -y (up)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, -oldGravity.y);
                                    updir = Direction.z;

                                    cameraTarget.x = 270;
                                    cameraTarget.y = 180;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -y (down)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 180f, transform.rotation.eulerAngles.z + 180);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.negz;

                                    cameraTarget.x += 90;
                                    cameraTarget.x += 180;
                                    cameraTarget.x -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -z ========
                        case Direction.negz:
                            // If going in negative negz, which is positive z velocity (left)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in negative z (right)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in -y (up)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z + 180);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negx;

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 180;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -y (down)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z + 180f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.x;

                                    //cameraTarget.x += 90;
                                    //cameraTarget.y -= 180;
                                    //cameraTarget.z += 180;
                                    cameraTarget.x = 90;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 0;

                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown right direction");
                            break;
                    }
                    break;



                // ======== If up direction is -z ========
                case Direction.negz:
                    switch (rightdir)
                    {
                        // ======== If right direction is x ========
                        case Direction.x:
                            // If going in negative x (left)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.z - 90f);
                                    transform.localEulerAngles = new Vector3(0, 90f, 270f);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    //cameraTarget.x += 90;
                                    //cameraTarget.y += 90;
                                    //cameraTarget.z -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in x (right)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z + 90);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in -z (up)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, -oldGravity.z, oldGravity.y);
                                    updir = Direction.y;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -z (down)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localEulerAngles = new Vector3(0f, 180f, 180f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.negy;

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 180;
                                    cameraTarget.z -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is y ========
                        case Direction.y:
                            // If going in negative y (left)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z - 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;

                                    rotateCamera = true;
                                    rightdir = Direction.x;
                                }
                            }

                            // If going in y (right)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    //cameraTarget.x += 90;
                                    //cameraTarget.y -= 90;
                                    //cameraTarget.z -= 90;
                                    cameraTarget.x = 90;
                                    cameraTarget.y = 180;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }

                            // If going in -z (up)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negx;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -z (down)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.x;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -x ========
                        case Direction.negx:
                            // If going in negative negx, which is positive x velocity (left)
                            if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.x);

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y += 90;
                                    //cameraTarget.z += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 270;
                                    cameraTarget.z = 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in negative x (right)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y -= 90;
                                    //cameraTarget.z -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in -z (up)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, oldGravity.z, oldGravity.y);
                                    updir = Direction.negy;

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 180;
                                    cameraTarget.z -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -z (down)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localEulerAngles = new Vector3(0, 180, 0);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.x, -oldGravity.z, oldGravity.y);
                                    updir = Direction.y;

                                    //cameraTarget.x += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 180;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -y ========
                        case Direction.negy:
                            // If going in negative negy, which is positive y velocity (left)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negx;
                                }
                            }


                            // If going in negative y (right)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(-myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;

                                    rightdir = Direction.x;
                                }
                            }

                            // If going in -z (up)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(-myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(-oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.x;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -z (down)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negx;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown direction");
                            break;
                    }
                    break;


                // ======== If up direction is -x ========
                case Direction.negx:
                    switch (rightdir)
                    {
                        // ======== If right direction is z ========
                        case Direction.z:
                            // If going in negative z (left)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y += 90;
                                    //cameraTarget.z += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 0;
                                    cameraTarget.z = 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in z (right)
                            else if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y - 90, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z -= 90;
                                    //cameraTarget.x = 0;
                                    //cameraTarget.y = 180;
                                    //cameraTarget.z = 270;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in -x (up)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 180f, transform.rotation.eulerAngles.z + 180f);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negy;

                                    //cameraTarget.x -= 90;
                                    //cameraTarget.y -= 180;
                                    //cameraTarget.z += 180;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 90;
                                    cameraTarget.z = 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -x (down)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, -oldGravity.x, oldGravity.y);
                                    updir = Direction.y;

                                    //cameraTarget.x -= 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 270;
                                    cameraTarget.z = 0;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is y ========
                        case Direction.y:
                            // If going in negative y (left)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    //transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localEulerAngles = new Vector3(270, 90, 0);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;

                                    rotateCamera = true;
                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in y (right)
                            else if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z - 90);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z); ;
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }

                            // If going in -x (up)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, -oldGravity.x);
                                    updir = Direction.z;

                                    cameraTarget.y += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negz;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -z ========
                        case Direction.negz:
                            // If going in negative negz, which is positive z velocity (left)
                            if (myRB.velocity.z > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z - 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z -= 90;
                                    rotateCamera = true;
                                    rightdir = Direction.negy;
                                }
                            }

                            // If going in negative -z (right)
                            if (myRB.velocity.z < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.z < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, -turnPoint);
                                    myRB.velocity = new Vector3(myRB.velocity.x, -myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.y;
                                }
                            }

                            // If going in -x (up)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, -myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, -oldGravity.x, oldGravity.z);
                                    updir = Direction.y;

                                    cameraTarget.x += 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative x (down)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 180f, transform.rotation.eulerAngles.z - 180f);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.y, myRB.velocity.x, myRB.velocity.z);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.y, oldGravity.x, oldGravity.z);
                                    updir = Direction.negy;

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 180;
                                    cameraTarget.z -= 180;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        // ======== If right direction is -y ========
                        case Direction.negy:
                            // If going in negative negy, which is positive y velocity (left)
                            if (myRB.velocity.y > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y > turnPoint)
                                {
                                    Debug.Log("Go to the left");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x + 90f, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, -myRB.velocity.y);

                                    cameraTarget.x += 90;
                                    cameraTarget.y += 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;
                                    rightdir = Direction.z;
                                }
                            }


                            // If going in negative y (right)
                            if (myRB.velocity.y < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.y < -turnPoint)
                                {
                                    Debug.Log("Go to the right");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x - 90f, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z + 90f);
                                    transform.localPosition = new Vector3(transform.localPosition.x, -turnPoint, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.z, myRB.velocity.y);

                                    cameraTarget.x -= 90;
                                    cameraTarget.y -= 90;
                                    cameraTarget.z += 90;
                                    rotateCamera = true;

                                    rightdir = Direction.negz;
                                }
                            }

                            // If going in -x (up)
                            if (myRB.velocity.x < 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x < -turnPoint)
                                {
                                    Debug.Log("Go to the top");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y - 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(-turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, oldGravity.x);
                                    updir = Direction.negz;

                                    cameraTarget.y -= 90;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }

                            // If going in negative -x (down)
                            else if (myRB.velocity.x > 0f)
                            {
                                // If on cube edge
                                if (transform.localPosition.x > turnPoint)
                                {
                                    Debug.Log("Go to the bottom");
                                    transform.localEulerAngles = new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y + 90f, transform.rotation.eulerAngles.z);
                                    transform.localPosition = new Vector3(turnPoint, transform.localPosition.y, transform.localPosition.z);
                                    myRB.velocity = new Vector3(myRB.velocity.z, myRB.velocity.y, -myRB.velocity.x);

                                    Vector3 oldGravity = Physics.gravity;
                                    Physics.gravity = new Vector3(oldGravity.z, oldGravity.y, -oldGravity.x);
                                    updir = Direction.z;

                                    //cameraTarget.y += 90;
                                    cameraTarget.x = 0;
                                    cameraTarget.y = 270;
                                    cameraTarget.z = 270;
                                    rotateCamera = true;
                                    Debug.Log(Physics.gravity);
                                }
                            }
                            break;

                        default:
                            Debug.Log("Unknown right direction");
                            break;
                    }
                    break;

                default:
                    Debug.Log("Unknown up direction");
                    break;

            }


        }




        if (Time.time > nextCheck)
        {
            nextCheck = Time.time + (1f / checksPerSecond);
            grounded = IsGrounded();
            moving = IsMoving();
            canStand = CanStand();
        }

        if (lastGrounded != grounded)
        {
            //This sound will play when jumping or when landing
            if (grounded)
            {
                Footstep();
                nextFootstep = Time.time + (0.2f);
            }
            else
            {
                if (nextFootstep < Time.time - (0.05f))
                {
                    Footstep();
                }
            }
            lastGrounded = grounded;
            if (lastGrounded == true)
            {
                landedYPos = transform.position.y;
                if (jumpedYPos > landedYPos)
                {
                    float distanceFell = jumpedYPos - landedYPos;
                    if (distanceFell > fallThreshold)
                    {
                        if (distanceFell * fallDamageMultiplier > 1.5f)
                        {
                            distanceFell -= fallThreshold;
                            //Here is where you will do your fall damage calculation
                            //playerHealth -= Mathf.RoundToInt(distanceFell * fallDamageMultiplier);
                        }
                    }
                }
            }
            else
            {
                lastAiredPos = transform.position.y;
                jumpsDone = 0;
            }
        }
        if (!grounded)
        {
            if (transform.position.y > lastAiredPos)
            {
                lastAiredPos = transform.position.y;
            }
            else
            {
                jumpedYPos = lastAiredPos;
            }
        }
        //Stamina regeneration for running
        if (ableToRun)
        {
            if (stamina < maxStamina && Time.time > nextStamina)
            {
                nextStamina = Time.time + (1f / staminaRegenPerSecond);
                stamina += 1;
            }
        }
        //Footstep sounds when moving
        if (moving && Time.time > nextFootstep && grounded)
        {
            float mp = Random.Range(0.8f, 1.2f);
            nextFootstep = Time.time + ((3.5f / currentSpeed) * mp) / footstepSpeed;
            Footstep();
        }

        if (Input.GetButtonDown("Fire2") && Time.time > respawnCooldown)
        {
            RespawnAtSpawnPoint();
            respawnCooldown = Time.time + 1;
        }

        //Where the jump function happens
        if (controllerAble)
        {
            if (Input.GetButtonDown("Jump") && Input.GetAxisRaw("Vertical") != -1)
            {
                Jump();
            }
            //else if (Input.GetButtonDown("Jump") && Input.GetAxisRaw("Vertical") == -1)
            //{
            //    JumpDown();
            //}
            else if (Input.GetButton("Jump"))
            {
                // Add upward velocity when holding down jump
                if (jumpValue < maxJumpValue && canJumpHigher)
                {
                    float jumpAmnt = increaseRate * (Time.deltaTime * 60f);
                    jumpValue += jumpAmnt;

                    if (jumpValue < maxJumpValue)
                    {
                        myRB.AddForce(GetCurrentUpVector() * jumpAmnt, ForceMode.Acceleration);
                    }
                    else
                    {
                        myRB.AddForce(GetCurrentUpVector() * (jumpValue - maxJumpValue), ForceMode.Acceleration);
                        canJumpHigher = false;
                    }


                }
            }
        }

        if (grabbing)
        {
            if (Input.GetButtonDown("Jump") && Input.GetAxisRaw("Vertical") != -1)
            {
                grabbing = false;
                controllerAble = true;
                myRB.useGravity = true;
                myRB.isKinematic = false;
                Jump();
                jumpsDone = 0;
            }
            else if (Input.GetButtonDown("Jump") && Input.GetAxisRaw("Vertical") == -1)
            {
                grabbing = false;
                controllerAble = true;
                myRB.useGravity = true;
                myRB.isKinematic = false;
                JumpDown();
                jumpsDone = 0;
            }
        }


        if (Input.GetButtonUp("Jump") && controllerAble)
        {
            canJumpHigher = false;
        }


    }
    public void Footstep()
    {
        //networkView.RPC("Step", RPCMode.All);
        Step();
    }
    [RPC]
    void Step()
    {
        if (audioSource && footstepSound)
        {
            audioSource.pitch = Random.Range(0.7f, 1.3f);
            audioSource.volume = 1.0f;
            audioSource.maxDistance = 30f;
            audioSource.PlayOneShot(footstepSound);
        }
    }
    [RPC]
    void CrouchState(bool newCrouch)
    {
        //croucher.crouching = newCrouch;
    }
    void FixedUpdate()
    {
        currentSpeed = Mathf.Round(myRB.velocity.magnitude);
        speed = walkSpeed;

        if (lastCrouching != crouching)
        {
            lastCrouching = crouching;
            //This can be an rpc call
            //networkView.RPC("CrouchState", RPCMode.All, crouching);
            CrouchState(crouching);
        }

        if (Input.GetKey(KeyCode.LeftControl) || !canStand)
        {
            if (grounded)
            {
                speed = crouchSpeed * speedMultiplier;
            }

            crouching = true;
        }
        else if (Input.GetKey(KeyCode.LeftShift) && canStand && stamina > 0 && ableToRun)
        {
            //Running
            if (grounded)
            {
                stamina -= 1;
                speed = runSpeed * speedMultiplier;
            }
            else
            {
                speed = walkSpeed * speedMultiplier;
            }
            crouching = false;
        }
        else
        {
            if (!canStand)
            {
                if (grounded)
                {
                    speed = crouchSpeed * speedMultiplier;
                }
                crouching = true;
            }
            else
            {
                speed = walkSpeed * speedMultiplier;

                crouching = false;
            }
        }

        if (grounded)
        {
            if (controllerAble)
            {


                if (light.enabled)
                {
                    acceleration = groundAccelerator;
                }
                else
                {
                    acceleration = groundAcceleratorInDark;
                }


            }
        }
        else
        {
            if (controllerAble)
            {
                acceleration = airAccelerator;
                speed = currentSpeed;
            }
            else
            {
                acceleration = 0.1f;
            }
        }
        //Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, Input.GetAxisRaw("Vertical"));
        //2D Version:



        Vector3 input = new Vector3(Input.GetAxisRaw("Horizontal"), 0f, 0f);

        Vector3 targetVelocity = input;

        targetVelocity = transform.TransformDirection(targetVelocity.normalized) * speed;
        velocityChange = (targetVelocity - myRB.velocity);


        switch (updir)
        {
            case Direction.y:
                velocityChange.x = Mathf.Clamp(velocityChange.x, -acceleration, acceleration);
                velocityChange.z = Mathf.Clamp(velocityChange.z, -acceleration, acceleration);
                velocityChange.y = 0f;
                break;

            case Direction.x:
                velocityChange.x = 0f;
                velocityChange.z = Mathf.Clamp(velocityChange.z, -acceleration, acceleration);
                velocityChange.y = Mathf.Clamp(velocityChange.y, -acceleration, acceleration);
                break;

            case Direction.z:
                velocityChange.x = Mathf.Clamp(velocityChange.x, -acceleration, acceleration);
                velocityChange.z = 0f;
                velocityChange.y = Mathf.Clamp(velocityChange.y, -acceleration, acceleration);
                break;

            case Direction.negy:
                velocityChange.x = Mathf.Clamp(velocityChange.x, -acceleration, acceleration);
                velocityChange.z = Mathf.Clamp(velocityChange.z, -acceleration, acceleration);
                velocityChange.y = 0f;
                break;

            case Direction.negx:
                velocityChange.x = 0f;
                velocityChange.z = Mathf.Clamp(velocityChange.z, -acceleration, acceleration);
                velocityChange.y = Mathf.Clamp(velocityChange.y, -acceleration, acceleration);
                break;

            case Direction.negz:
                velocityChange.x = Mathf.Clamp(velocityChange.x, -acceleration, acceleration);
                velocityChange.z = 0f;
                velocityChange.y = Mathf.Clamp(velocityChange.y, -acceleration, acceleration);
                break;

            default:
                Debug.Log("waat");
                break;
        }




        if (controllerAble)
        {

            // Rotate character to face the direction he is walking
            if (input.x < 0)
            {
                characterGraphics.transform.localRotation = Quaternion.Euler(new Vector3(0f, 180f, 0f));
                //characterGraphics.transform.rotation = Quaternion.AngleAxis(180f, GetCurrentUpVector());
                //Vector3 theScale = characterGraphics.transform.localScale;
                //theScale.x = playerScaleX * -1;
                ////theScale.z = playerScaleZ;
                //characterGraphics.transform.localScale = theScale;
            }
            else if (input.x > 0)
            {
                characterGraphics.transform.localRotation = Quaternion.Euler(Vector3.zero);
                //characterGraphics.transform.rotation = Quaternion.AngleAxis(0f, GetCurrentUpVector());
                //Vector3 theScale = characterGraphics.transform.localScale;
                //theScale.x = playerScaleX;
                ////theScale.z = playerScaleZ;
                //characterGraphics.transform.localScale = theScale;
            }


            //Speed limit for diagonal walking
            if (targetVelocity.sqrMagnitude > (speed * speed))
            {
                targetVelocity = targetVelocity.normalized * speed;
            }
            //Speed limit for falling


            switch (updir)
            {
                case Direction.y:
                    if (myRB.velocity.y < -maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(myRB.velocity.x, -maxFallSpeed, myRB.velocity.z);
                    }
                    break;

                case Direction.z:
                    if (myRB.velocity.z < -maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.y, -maxFallSpeed);
                    }
                    break;

                case Direction.x:
                    if (myRB.velocity.x < -maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(-maxFallSpeed, myRB.velocity.y, myRB.velocity.z);
                    }
                    break;

                case Direction.negy:
                    if (myRB.velocity.y > maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(myRB.velocity.x, maxFallSpeed, myRB.velocity.z);
                    }
                    break;

                case Direction.negz:
                    if (myRB.velocity.z > maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(myRB.velocity.x, myRB.velocity.y, maxFallSpeed);
                    }
                    break;

                case Direction.negx:
                    if (myRB.velocity.y > maxFallSpeed)
                    {
                        myRB.velocity = new Vector3(maxFallSpeed, myRB.velocity.y, myRB.velocity.z);
                    }
                    break;

                default:
                    break;
            }


            if (grounded)
            {
                //If not pressing button, reduce speed gradually
                if (input.x != 0 || input.z != 0)
                {
                    //HERE IS FUCKING MOVMENT HANDLED

                    myRB.AddForce(velocityChange, ForceMode.VelocityChange);
                }
                else
                {
                    ReduceSideVelocity(6);

                }

            }
            else
            {
                //If in mid air then change movement speed if actually trying to move
                if (input.x != 0 || input.z != 0)
                {

                    // If max speed has been reached, return
                    switch (updir)
                    {
                        case Direction.y:
                            if (myRB.velocity.x + velocityChange.x >= maxHorizontalSpeed || myRB.velocity.x + velocityChange.x <= -maxHorizontalSpeed ||
                                myRB.velocity.z + velocityChange.z >= maxHorizontalSpeed || myRB.velocity.z + velocityChange.z <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        case Direction.z:
                            if (myRB.velocity.x + velocityChange.x >= maxHorizontalSpeed || myRB.velocity.x + velocityChange.x <= -maxHorizontalSpeed ||
                                myRB.velocity.y + velocityChange.y >= maxHorizontalSpeed || myRB.velocity.y + velocityChange.y <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        case Direction.x:
                            if (myRB.velocity.z + velocityChange.z >= maxHorizontalSpeed || myRB.velocity.z + velocityChange.z <= -maxHorizontalSpeed ||
                                myRB.velocity.y + velocityChange.y >= maxHorizontalSpeed || myRB.velocity.y + velocityChange.y <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        case Direction.negy:
                            if (myRB.velocity.x + velocityChange.x >= maxHorizontalSpeed || myRB.velocity.x + velocityChange.x <= -maxHorizontalSpeed ||
                                myRB.velocity.z + velocityChange.z >= maxHorizontalSpeed || myRB.velocity.z + velocityChange.z <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        case Direction.negz:
                            if (myRB.velocity.x + velocityChange.x >= maxHorizontalSpeed || myRB.velocity.x + velocityChange.x <= -maxHorizontalSpeed ||
                                myRB.velocity.y + velocityChange.y >= maxHorizontalSpeed || myRB.velocity.y + velocityChange.y <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        case Direction.negx:
                            if (myRB.velocity.z + velocityChange.z >= maxHorizontalSpeed || myRB.velocity.z + velocityChange.z <= -maxHorizontalSpeed ||
                                myRB.velocity.y + velocityChange.y >= maxHorizontalSpeed || myRB.velocity.y + velocityChange.y <= -maxHorizontalSpeed)
                            {
                                return;
                            }

                            break;

                        default:
                            Debug.Log("Unknown up direction");
                            return;
                    }


                    // Else add the force
                    myRB.AddForce(velocityChange * 2f, ForceMode.Acceleration);
                }
                // Reduce velocity while in air and not moving
                else
                {
                    ReduceSideVelocity(70);
                }
            }

            // Uncomment if we need to move in a 3d space
            /*
            if (targetVelocity != Vector3.zero)
            {
                Quaternion rot = new Quaternion();
                rot.SetLookRotation(targetVelocity);
                characterGraphics.transform.rotation = rot;
            }
            */


        }
        else
        {
            //If the player isnt supposed to move, the player movement on x and z axis is 0
            targetVelocity = Vector3.zero;
            //myRB.velocity = new Vector3(0, myRB.velocity.y, 0);
        }
    }
    public bool IsMoving()
    {
        float minSpeed = 0.5f;
        return myRB.velocity.magnitude > minSpeed;
    }
    public bool CanStand()
    {
        //croucher.defHeight is the original height of the capsule collider
        //because when crouching the capsule collider height changes
        //divided by 2 because the cast starts from the center of the player and not the top
        //float castDistance = croucher.defHeight / 2f + 0.1f;

        //Vector3 centerCast = new Vector3(transform.position.x, croucher.globalYPosition, transform.position.z);

        //return !Physics.Raycast(centerCast, transform.up, castDistance);
        return true;
    }
    public bool IsGrounded()
    {
        //float castRadius = capsule.radius - 0.1f;
        //float castDistance = capsule.height / 2f + 0.2f;

        // If player has an up or down velocity, he is falling or jumping, and is not grounded
        // Checking for 0.0f velocity does not work, since player often has a little downwards velocity even when grounded
        switch (updir)
        {
            case Direction.y:
            case Direction.negy:
                if (Mathf.Abs(myRB.velocity.y) > 0.1f)
                {
                    return false;
                }
                break;

            case Direction.x:
            case Direction.negx:
                if (Mathf.Abs(myRB.velocity.x) > 0.1f)
                {
                    return false;
                }
                break;

            case Direction.z:
            case Direction.negz:
                if (Mathf.Abs(myRB.velocity.z) > 0.1f)
                {
                    return false;
                }
                break;

            default:
                Debug.LogError("No up direction");
                break;
        }

        float castRadius = capsule.radius * 0.8f;
        float castDistance = 0f;
        float percentOfDistance = 0.8f;

        //1 cast in the middle, and 4 more casts on the edges of the collider
        Vector3 leftCast = Vector3.zero;
        Vector3 rightCast = Vector3.zero;
        Vector3 frontCast = Vector3.zero;
        Vector3 backCast = Vector3.zero;
        Vector3 centerCast = Vector3.zero;

        Vector3 castPoint = feetPosition.position;
        //Vector3 castPoint = transform.position;

        switch (updir)
        {
            case Direction.y:
            case Direction.negy:
                castDistance = Mathf.Abs(castPoint.y - transform.position.y) * percentOfDistance;
                leftCast = new Vector3(castPoint.x - castRadius, castPoint.y, castPoint.z);
                rightCast = new Vector3(castPoint.x + castRadius, castPoint.y, castPoint.z);
                frontCast = new Vector3(castPoint.x, castPoint.y, castPoint.z + castRadius);
                backCast = new Vector3(castPoint.x, castPoint.y, castPoint.z - castRadius);
                centerCast = castPoint;
                break;

            case Direction.z:
            case Direction.negz:
                castDistance = Mathf.Abs(castPoint.z - transform.position.z) * percentOfDistance;
                leftCast = new Vector3(castPoint.x - castRadius, castPoint.y, castPoint.z);
                rightCast = new Vector3(castPoint.x + castRadius, castPoint.y, castPoint.z);
                frontCast = new Vector3(castPoint.x, castPoint.y + castRadius, castPoint.z);
                backCast = new Vector3(castPoint.x, castPoint.y - castRadius, castPoint.z);
                centerCast = castPoint;
                break;

            case Direction.x:
            case Direction.negx:
                castDistance = Mathf.Abs(castPoint.x - transform.position.x) * percentOfDistance;
                leftCast = new Vector3(castPoint.x, castPoint.y - castRadius, castPoint.z);
                rightCast = new Vector3(castPoint.x, castPoint.y + castRadius, castPoint.z);
                frontCast = new Vector3(castPoint.x, castPoint.y, castPoint.z + castRadius);
                backCast = new Vector3(castPoint.x, castPoint.y, castPoint.z - castRadius);
                centerCast = castPoint;
                break;

            default:
                Debug.Log("Unknown updir");
                break;
        }

        Vector3 dir = -GetCurrentUpVector();

        return (Physics.Raycast(leftCast, dir, castDistance) || Physics.Raycast(rightCast, dir, castDistance) ||
                        Physics.Raycast(frontCast, dir, castDistance) || Physics.Raycast(backCast, dir, castDistance) ||
                        Physics.Raycast(centerCast, dir, castDistance));




    }
    float CalculateJumpVerticalSpeed()
    {
        return Mathf.Sqrt(jumpHeight * 20f);
    }
    void OnTriggerStay(Collider what)
    {
        if (what.name == "Ladder")
        {
            myRB.velocity = Vector3.Lerp(myRB.velocity, new Vector3(myRB.velocity.x / 4f, climbSpeed, myRB.velocity.z / 4f), Time.deltaTime * 15f);
        }

    }
    void OnTriggerExit(Collider what)
    {
        if (what.name == "Ladder")
        {
            jumpedYPos = transform.position.y;
        }

        if (what.gameObject.tag == "SwapZone")
        {
            canSwap = false;
        }
    }
    void OnTriggerEnter(Collider what)
    {
        if (what.gameObject.tag == "SwapZone")
        {
            canSwap = true;
        }
    }
    void OnCollisionEnter(Collision what)
    {

        if (what.gameObject.tag == "MovingPlatform" && grounded)
        {
            transform.parent = what.transform;
        }
    }

    void OnCollisionStay(Collision what)
    {
        if (jumpDown)
        {
            if (what.gameObject.tag == "JumpThrough")
            {
                Physics.IgnoreCollision(transform.collider, what.collider);
                Physics.IgnoreCollision(headCollider.collider, what.collider);
                //myRB.velocity += GetJumpVerticalVelocity() * maxFallSpeed / 10;
                //myRB.velocity += GetJumpVerticalVelocity();
            }

            jumpDown = false;
        }
    }

    void OnCollisionExit(Collision what)
    {
        if (what.gameObject.tag == "MovingPlatform")
        {
            transform.parent = currentLevel.transform;
        }

        //if (what.gameObject.tag == "JumpThrough")
        //{
        //    Physics.IgnoreCollision(transform.collider, what.collider, false);
        //    Physics.IgnoreCollision(headCollider.collider, what.collider, false);
        //}

    }

    public int getPlayerFacePos()
    {
        //return onCubeFace;
        return -1;
    }

    public Vector3 GetCurrentUpVector()
    {
        Vector3 tempvec = Vector3.zero;

        switch (updir)
        {
            case Direction.y:
                tempvec.x = 0;
                tempvec.y = 1;
                tempvec.z = 0;

                break;

            case Direction.z:
                tempvec.x = 0;
                tempvec.y = 0;
                tempvec.z = 1;

                break;

            case Direction.x:
                tempvec.x = 1;
                tempvec.y = 0;
                tempvec.z = 0;

                break;

            case Direction.negy:
                tempvec.x = 0;
                tempvec.y = -1;
                tempvec.z = 0;

                break;

            case Direction.negz:
                tempvec.x = 0;
                tempvec.y = 0;
                tempvec.z = -1;

                break;

            case Direction.negx:
                tempvec.x = -1;
                tempvec.y = 0;
                tempvec.z = 0;

                break;

            default:
                Debug.Log("Unknown up direction, cannot ground");
                break;
        }

        return tempvec;
    }


    public int getUpDir()
    {
        return (int)updir;
    }

    public float getTurnPoint()
    {
        return turnPoint;
    }

    public void CalculateAndSetCubeRotation(bool lerp)
    {
        float percentToRight = 0f;
        float percentToTop = 0f;
        Quaternion gimbalQuaternion = Quaternion.identity;


        switch (updir)
        {
            case Direction.y:
                percentToTop = transform.localPosition.y / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);


                switch (rightdir)
                {
                    case Direction.x:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.negx:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.z:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.negz:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, transform.up);
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;


            case Direction.negy:
                percentToTop = transform.localPosition.y / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(-maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);


                switch (rightdir)
                {
                    case Direction.x:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.negx:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.z:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, transform.up);
                        break;

                    case Direction.negz:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, transform.up);
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;

            case Direction.x:
                // GIMBAL LOCK! but quaternions kicked its ass
                percentToTop = transform.localPosition.x / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);


                //gimbalLock = true;

                switch (rightdir)
                {
                    case Direction.y:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negy:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.z:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negz:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;

            case Direction.negx:
                percentToTop = transform.localPosition.x / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(-maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);


                switch (rightdir)
                {
                    case Direction.y:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negy:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.z:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negz:
                        percentToRight = transform.localPosition.z / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;

            case Direction.z:
                percentToTop = transform.localPosition.z / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);



                switch (rightdir)
                {
                    case Direction.y:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negy:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.x:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negx:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;

            case Direction.negz:
                percentToTop = transform.localPosition.z / turnPoint;
                gimbalQuaternion = Quaternion.AngleAxis(-maxRotation * percentToTop, transform.right) * Quaternion.Euler(cameraTarget);



                switch (rightdir)
                {
                    case Direction.y:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negy:
                        percentToRight = transform.localPosition.y / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.x:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(-maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    case Direction.negx:
                        percentToRight = transform.localPosition.x / turnPoint;
                        gimbalQuaternion *= Quaternion.AngleAxis(maxRotation * percentToRight, new Vector3(0, 1, 0));
                        break;

                    default:
                        Debug.Log("Unknown right dir");
                        break;
                }
                break;


            default:
                Debug.Log("Unknown dir");
                break;
        }

        if (lerp)
        {

            //float axisVal = (Input.GetAxisRaw("Vertical") / 10);
            //Debug.Log(axisVal);

            everythingRotating.transform.rotation = Quaternion.Lerp(everythingRotating.transform.rotation, gimbalQuaternion, rotationSpeed * Time.deltaTime);
            //nextLevelCube.transform.rotation = Quaternion.Lerp(everythingRotating.transform.rotation, gimbalQuaternion, rotationSpeed * Time.deltaTime);
            //nextLevelCube.transform.localScale = new Vector3(	nextLevelCube.transform.localScale.x + axisVal,
            //                                                    nextLevelCube.transform.localScale.y + axisVal,
            //                                                    nextLevelCube.transform.localScale.z + axisVal);
            //nextLevelCube.transform.localScale = new Vector3(everythingRotating.transform.rotation.x, everythingRotating.transform.rotation.y, everythingRotating.transform.rotation.z);;



        }
        else
        {
            everythingRotating.transform.rotation = gimbalQuaternion;
        }

    }

    public void SetCurrentLevel(GameObject level, Transform spawnPoint)
    //public void SetCurrentLevel(GameObject level)
    {
        currentLevel = level;
        if (currentLevel == null)
        {
            transform.parent = null;
            return;
        }

        // Parent to spawn object and scale according to its size and position
        //transform.parent = currentLevel.transform.FindChild("PlayerSpawn");
        transform.parent = spawnPoint;
        transform.position = transform.parent.position;
        transform.rotation = transform.parent.rotation;
        transform.localScale = new Vector3(1f, 1f, 1f);

        myRB.velocity = Vector3.zero;

        cameraTarget = spawnPoint.localRotation.eulerAngles;
        Physics.gravity = spawnPoint.up * gravity;

        // Since unity is strange, and sometimes we get super small rotations for no reason like spawnpoint.right = (-0.0000000000000001, 0, 0.99999), determine updir and rightdir by which value is the highest.
        float highest = 0;
        Direction dir = Direction.x;


        // Up vector
        if (Mathf.Abs(spawnPoint.up.x) > highest)
        {
            highest = Mathf.Abs(spawnPoint.up.x);
            if (spawnPoint.up.x < 0)
            {
                dir = Direction.negx;
            }
            else
            {
                dir = Direction.x;
            }
        }
        if (Mathf.Abs(spawnPoint.up.y) > highest)
        {
            highest = Mathf.Abs(spawnPoint.up.y);
            if (spawnPoint.up.y < 0)
            {
                dir = Direction.negy;
            }
            else
            {
                dir = Direction.y;
            }
        }
        if (Mathf.Abs(spawnPoint.up.z) > highest)
        {
            highest = Mathf.Abs(spawnPoint.up.z);
            if (spawnPoint.up.z < 0)
            {
                dir = Direction.negz;
            }
            else
            {
                dir = Direction.z;
            }
        }

        updir = dir;

        highest = 0;

        // Right vector
        if (Mathf.Abs(spawnPoint.right.x) > highest)
        {
            highest = Mathf.Abs(spawnPoint.right.x);
            if (spawnPoint.right.x < 0)
            {
                dir = Direction.negx;
            }
            else
            {
                dir = Direction.x;
            }
        }
        if (Mathf.Abs(spawnPoint.right.y) > highest)
        {
            highest = Mathf.Abs(spawnPoint.right.y);
            if (spawnPoint.right.y < 0)
            {
                dir = Direction.negy;
            }
            else
            {
                dir = Direction.y;
            }
        }
        if (Mathf.Abs(spawnPoint.right.z) > highest)
        {
            highest = Mathf.Abs(spawnPoint.right.z);
            if (spawnPoint.right.z < 0)
            {
                dir = Direction.negz;
            }
            else
            {
                dir = Direction.z;
            }
        }

        rightdir = dir;

        // Determine updir
        //if (spawnPoint.up.x == 1)
        //{
        //    updir = Direction.x;
        //    Debug.Log("Opp x");
        //}
        //else if (spawnPoint.up.x == -1)
        //{
        //    updir = Direction.negx;
        //    Debug.Log("Opp -x");
        //}
        //else if (spawnPoint.up.y == 1)
        //{
        //    updir = Direction.y;
        //    Debug.Log("Opp y");
        //}
        //else if (spawnPoint.up.y == -1)
        //{
        //    updir = Direction.negy;
        //    Debug.Log("Opp -y");
        //}
        //else if (spawnPoint.up.z == 1)
        //{
        //    updir = Direction.z;
        //    Debug.Log("Opp z");
        //}
        //else if (spawnPoint.up.z == -1)
        //{
        //    updir = Direction.negz;
        //    Debug.Log("Opp -z");
        //}
        //else
        //{
        //    Debug.LogError("Dont know what is up!");
        //    Debug.LogError("SpawnPoint up: " + spawnPoint.up);
        //}


        // Determine rightdir
        //if (spawnPoint.right.x == 1)
        //{
        //    rightdir = Direction.x;
        //    Debug.Log("Right x");
        //}
        //else if (spawnPoint.right.x == -1)
        //{
        //    rightdir = Direction.negx;
        //    Debug.Log("Right -x");
        //}
        //else if (spawnPoint.right.y == 1)
        //{
        //    rightdir = Direction.y;
        //    Debug.Log("Right y");
        //}
        //else if (spawnPoint.right.y == -1)
        //{
        //    rightdir = Direction.negy;
        //    Debug.Log("Right -y");
        //}
        //else if (spawnPoint.right.z == 1)
        //{
        //    rightdir = Direction.z;
        //    Debug.Log("Right z");
        //}
        //else if (spawnPoint.right.z == -1)
        //{
        //    rightdir = Direction.negz;
        //    Debug.Log("Right -z");
        //}
        //else
        //{
        //    Debug.LogError("Dont know what is right!");
        //    Debug.LogError("SpawnPoint right: " + spawnPoint.right);
        //    Debug.Log(spawnPoint.right.x);
        //    Debug.Log(spawnPoint.right.y);
        //    Debug.Log(spawnPoint.right.z);
        //}


        // Set the cube as parent, because it might be handy later?
        transform.parent = currentLevel.transform;

        this.spawnPoint = spawnPoint;
        spawnCameraRotation = cameraTarget;
        spawnUpDir = updir;
        spawnRightDir = rightdir;
        spawnGravity = Physics.gravity;
    }

    public bool GetGrounded()
    {
        return grounded;
    }

    public void RespawnAtSpawnPoint()
    {
        transform.position = this.spawnPoint.position;
        transform.rotation = this.spawnPoint.rotation;
        myRB.isKinematic = false;
        myRB.useGravity = true;

        myRB.velocity = Vector3.zero;

        cameraTarget = spawnCameraRotation;

        updir = spawnUpDir;
        rightdir = spawnRightDir;
        Physics.gravity = spawnGravity;

        controllerAble = true;
    }
}