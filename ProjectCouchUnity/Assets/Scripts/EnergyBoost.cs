﻿using UnityEngine;
using System.Collections;

public class EnergyBoost : MonoBehaviour {

    public float powerAmnt = 15f;

    LightHandler lightHandler;

	// Use this for initialization
	void Start () {
        lightHandler = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<LightHandler>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider what)
    {
        if (what.gameObject.tag == "Player")
        {
            lightHandler.IncreaseEnergy(powerAmnt);
            Destroy(gameObject);
        }
    }
}
