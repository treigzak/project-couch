﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {

    public int maxHealth = 100;
    public int currentHealth = 100;

    private float timeflashedLast = 0f;
    private float flashDelay = 0.5f;
    private bool flashing = false;
    private Light light;

    void Start()
    {
        currentHealth = maxHealth;
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
    }

    void Update()
    {
        if (flashing)
        {
            if (timeflashedLast + flashDelay < Time.time)
            {
                transform.FindChild("Character Graphics").GetComponent<MeshRenderer>().enabled = light.enabled;
                flashing = false;
            }
            
        }
    }


    public void TakeDamage(int amnt) {
        if (transform.tag == "Enemy")
        {
            if (!light.enabled)
            {
                timeflashedLast = Time.time;
                transform.FindChild("Character Graphics").GetComponent<MeshRenderer>().enabled = true;
                flashing = true;
            }
            
        }
        currentHealth -= amnt;
        if (currentHealth <= 0)
        {
            if (transform.tag == "Player")
            {
                currentHealth = maxHealth;
                transform.GetComponent<Controller>().RespawnAtSpawnPoint();
            }
            else 
            {
                Destroy(gameObject);
            }
            
        }
    }
}
