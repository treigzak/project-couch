﻿using UnityEngine;
using System.Collections;

public class LightHandler : MonoBehaviour {

    public float ambientVolumeWhenLight = 0.05f;
    public float ambientVolumeWhenDark = 0.15f;

    public float drainSpeed = 1f;
    public float turnOnCost = 10f;

    public float twirlSpeed = 2;

    public AudioClip noEnergy;
    public AudioClip increaseEnergy;
    public AudioClip switchSound;

    public GameObject beastPrefab;
    public GameObject[] beastStartPoints;

    private Light roomLight;
    private Light roomLightOff;
    public RectTransform energyForeground;

    private float lightEnergy = 100f;
    private float maxLightEnergy = 100f;
    private GameMaster gameMaster;
    private AudioSource effectPlayer;
    private NoiseAndGrain noiseEffect;
    private AudioSource ambientMusic;
    private Transform environment;
    private TwirlEffect twirlEffect;
    private float maxTwirlAngle;
    private float twirlAngle;

    [HideInInspector]
    public bool gameOn = false;


	// Use this for initialization
	void Start () {
        gameMaster = gameObject.GetComponent<GameMaster>();
        roomLight = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        roomLightOff = GameObject.FindGameObjectWithTag("MasterLightOff").GetComponent<Light>();
        effectPlayer = GameObject.FindGameObjectWithTag("VariousSoundfx").GetComponent<AudioSource>();
        noiseEffect = Camera.main.GetComponent<NoiseAndGrain>();
        ambientMusic = GameObject.FindGameObjectWithTag("MusicAtCamera").GetComponent<AudioSource>();
        ambientMusic.volume = ambientVolumeWhenLight;
        environment = GameObject.FindGameObjectWithTag("Environment").transform;
        twirlEffect = Camera.main.GetComponent<TwirlEffect>();
        maxTwirlAngle = twirlEffect.angle;
        twirlEffect.angle = 0f;
	}
	
	// Update is called once per frame
	void Update () {
        //if (!gameOn) {
        //    return;
        //}

        //turn light on/off
        if (Input.GetKeyDown(KeyCode.F) && ((lightEnergy >= turnOnCost) || roomLight.enabled)) {
            noiseEffect.enabled = !noiseEffect.enabled;
            twirlEffect.enabled = !twirlEffect.enabled;
            twirlEffect.angle = 0f;
            twirlAngle = Random.Range(-maxTwirlAngle, maxTwirlAngle);

            effectPlayer.PlayOneShot(switchSound);

            ambientMusic.volume = (roomLight.enabled) ? ambientVolumeWhenDark : ambientVolumeWhenLight;

            if(roomLight.enabled) {
                
                roomLight.enabled = false;
                roomLightOff.enabled = true;

                spawnMonster();
                

            }else{
                roomLight.enabled = true;
                roomLightOff.enabled = false;

                lightEnergy -= turnOnCost;
                
            }

        }

        if (roomLight.enabled)
        {
            
            lightEnergy -= drainSpeed * Time.deltaTime;
            

            if (lightEnergy <= 0)
            {
                lightEnergy = 0;
                roomLight.enabled = false;
                effectPlayer.PlayOneShot(noEnergy);
                noiseEffect.enabled = !noiseEffect.enabled;
                twirlEffect.enabled = !twirlEffect.enabled;

                spawnMonster();
                ambientMusic.volume = ambientVolumeWhenDark;
                
            }
            //energyForeground.localScale = new Vector3(lightEnergy / maxLightEnergy, 1, 1);

        }
        else
        {
            if (twirlEffect.angle < twirlAngle)
            {
                twirlEffect.angle += Time.deltaTime * twirlSpeed;
            }
            else if (twirlEffect.angle > twirlAngle)
            {
                twirlEffect.angle -= Time.deltaTime * twirlSpeed;
            }
        }

        
	}

    private void spawnMonster()
    {

        if (beastStartPoints.Length > 0)
        {
            int beastRandSpawn = Random.Range(0, beastStartPoints.Length);
            GameObject beast = (GameObject)Instantiate(beastPrefab, beastStartPoints[beastRandSpawn].transform.position, beastStartPoints[beastRandSpawn].transform.rotation);
            //beast.transform.parent = Camera.main.transform;
            beast.transform.parent = environment.transform;

            if (beastStartPoints[beastRandSpawn].transform.childCount > 0)
            {
                int beastRandStare = Random.Range(0, beastStartPoints[beastRandSpawn].transform.childCount);
                beast.GetComponent<BeastScript>().SetStarePoint(beastStartPoints[beastRandSpawn].transform.GetChild(beastRandStare));
            }
            else
            {
                Debug.LogError("There are no stare points for the selected beast spawn point");
            }

        }
        else
        {
            Debug.LogError("There are no spawnpoints for the beast!");
        }

    }

    public void IncreaseEnergy(float increaseAmnt)
    {
        effectPlayer.PlayOneShot(increaseEnergy);

        if (lightEnergy + increaseAmnt > maxLightEnergy)
        {
            lightEnergy = maxLightEnergy;
        }
        else
        {
            lightEnergy += increaseAmnt;
        }
        //energyForeground.localScale = new Vector3(lightEnergy / maxLightEnergy, 1, 1);
    }
}
