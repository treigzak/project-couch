﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SetMaterialAlphaBasedOnFace : MonoBehaviour {

    public GameObject playerObject;

    private Controller playerController;
    private bool hasNewTarget = false;
    private List<Color> targetColors = new List<Color>();
    private List<Color> originalColors = new List<Color>();
    private List<Transform> childLevelObjects = new List<Transform>();
    private int opposingFace;
    private int currentPlayerFacePos = -1;
    private bool hidden = false;

	// Use this for initialization
	void Start () {
        playerController = playerObject.GetComponent<Controller>();

        switch (transform.parent.name)
        {
            case "FrontSide":
                opposingFace = 2;
                break;

            case "RightSide":
                opposingFace = 3;
                break;

            case "BackSide":
                opposingFace = 0;
                break;

            case "LeftSide":
                opposingFace = 1;
                break;

            case "TopSide":
                opposingFace = 5;
                break;

            case "BottomSide":
                opposingFace = 4;
                break;

            default:
                Debug.Log("Error: Unknown parent name");
                break;
        }

        // Populate list with all the current children objects on that face and their original color
        foreach (Transform child in transform)
        {
            if (child.tag == "SwapZone") {
                continue;
            }
            childLevelObjects.Add(child);
            originalColors.Add(new Color(child.renderer.material.GetColor("_TintColor").r, child.renderer.material.GetColor("_TintColor").g, child.renderer.material.GetColor("_TintColor").b, child.renderer.material.GetColor("_TintColor").a));
        }

        currentPlayerFacePos = playerController.getPlayerFacePos();

        for (int i = 0; i < childLevelObjects.Count; i++)
        {
            if (currentPlayerFacePos == opposingFace)
            {
                Color invisibleColor = new Color(childLevelObjects[i].renderer.material.GetColor("_TintColor").r, childLevelObjects[i].renderer.material.GetColor("_TintColor").g, childLevelObjects[i].renderer.material.GetColor("_TintColor").b, 0f);
                targetColors.Add(invisibleColor);
                childLevelObjects[i].renderer.material.SetColor("_TintColor", targetColors[i]);
                hidden = true;
                
            }
            else {
                Color sameColor = new Color(childLevelObjects[i].renderer.material.GetColor("_TintColor").r, childLevelObjects[i].renderer.material.GetColor("_TintColor").g, childLevelObjects[i].renderer.material.GetColor("_TintColor").b, childLevelObjects[i].renderer.material.GetColor("_TintColor").a);
                targetColors.Add(sameColor);
            }
        }
	}

    void Update()
    {
        if (hasNewTarget)
        {
            for (int i = 0; i < childLevelObjects.Count; i++)
            {
                Color newColor = Color.Lerp(childLevelObjects[i].renderer.material.GetColor("_TintColor"), targetColors[i], 2f * Time.deltaTime);
                childLevelObjects[i].renderer.material.SetColor("_TintColor", newColor);

                //if (childLevelObjects[i].renderer.material.GetColor("_TintColor") == targetColors[i])
                if (targetColors[i].a != 0)
                {
                    if (childLevelObjects[i].renderer.material.GetColor("_TintColor").a / targetColors[i].a >= 0.85)
                    {
                        childLevelObjects[i].renderer.material.SetColor("_TintColor", targetColors[i]);
                        hasNewTarget = false;
                        if (targetColors[i].a == 0f)
                        {
                            hidden = true;
                        }
                        else
                        {
                            hidden = false;
                        }
                    }
                }
                else
                {
                    if (childLevelObjects[i].renderer.material.GetColor("_TintColor").a <= 0.05f)
                    {
                        childLevelObjects[i].renderer.material.SetColor("_TintColor", targetColors[i]);
                        hasNewTarget = false;
                        if (targetColors[i].a == 0f)
                        {
                            hidden = true;
                        }
                        else
                        {
                            hidden = false;
                        }
                    }
                }
                
            }
        }
    }

    void FixedUpdate()
    {
        int playerFacePos = playerController.getPlayerFacePos();
        if (playerFacePos != currentPlayerFacePos)
        {
            currentPlayerFacePos = playerFacePos;
            if (currentPlayerFacePos == opposingFace)
            {
                changeAlpha(true);
            }
            else {
                changeAlpha(false);
            }
            
        }
    }


    void changeAlpha(bool hide)
    {
        // Hide objects
        if (hide) {
            for (int i = 0; i < childLevelObjects.Count; i++)
            {
                if (childLevelObjects[i].renderer.material.GetColor("_TintColor").a != 0f)
                {
                    Color newTarget = new Color(targetColors[i].r, targetColors[i].g, targetColors[i].b, 0f);
                    targetColors[i] = newTarget;

                    hasNewTarget = true;
                }
            }
        }
        // Show objects
        else
        {
            for (int i = 0; i < childLevelObjects.Count; i++)
            {
                if (childLevelObjects[i].renderer.material.GetColor("_TintColor") != originalColors[i])
                {
                    Color newTarget = new Color(originalColors[i].r, originalColors[i].g, originalColors[i].b, originalColors[i].a);
                    targetColors[i] = newTarget;

                    hasNewTarget = true;
                }
            }
        }
    }

    public bool canFlick() {
        // Object can flicker if it is not currently fading, and is not hidden
        return !hasNewTarget && !hidden;
    }
}
