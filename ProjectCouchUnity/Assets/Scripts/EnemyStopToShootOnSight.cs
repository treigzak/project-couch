﻿using UnityEngine;
using System.Collections;

public class EnemyStopToShootOnSight : MonoBehaviour {

    EnemyStopToShootController enemyController;

	// Use this for initialization
	void Start () {
        enemyController = transform.parent.parent.GetComponent<EnemyStopToShootController>();
	}
	
	// Update is called once per frame
    //void Update () {
	
    //}

    //void OnTriggerStay(Collider what)
    //{
    //    // If player is in line of sight of the enemy
    //    if (what.gameObject.tag == "Player")
    //    {
            
    //    }
    //}
    void OnTriggerEnter(Collider what)
    {
        if (what.gameObject.tag == "Player")
        {
            enemyController.SetShootingState(true);
        }
    }
    void OnTriggerExit(Collider what)
    {
        if (what.gameObject.tag == "Player")
        {
            enemyController.SetShootingState(false);
        }
    }
}
