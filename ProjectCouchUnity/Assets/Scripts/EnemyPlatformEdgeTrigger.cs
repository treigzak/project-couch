﻿using UnityEngine;
using System.Collections;

public class EnemyPlatformEdgeTrigger : MonoBehaviour {

    void OnTriggerEnter(Collider what)
    {
        if (what.gameObject.tag == "Enemy")
        {
            what.GetComponent<EnemyStopToShootController>().TurnAround();
        }
    }
}
