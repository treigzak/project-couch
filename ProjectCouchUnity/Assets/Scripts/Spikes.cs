﻿using UnityEngine;
using System.Collections;

public class Spikes : MonoBehaviour {

    public int spikeDamage = 99999;
    GameMaster gm;
    

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
    }

    void OnTriggerStay(Collider what)
    {
        // If player is in trigger zone and pushes down and is not shooting
        if (what.gameObject.tag == "Player")
        {
            what.transform.GetComponent<Health>().TakeDamage(spikeDamage);
        }

    }
}
