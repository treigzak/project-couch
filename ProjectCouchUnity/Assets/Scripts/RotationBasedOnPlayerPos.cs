﻿using UnityEngine;
using System.Collections;

public class RotationBasedOnPlayerPos : MonoBehaviour {

    enum Direction { x, y, z, negx, negy, negz };

    public GameObject playerCharacter;
    public float rotationSpeed = 1f;

    private Controller playerController;
    private float turnpoint;

    
	void Start () {
        playerController = playerCharacter.GetComponent<Controller>();
        turnpoint = playerController.getTurnPoint();
	}
	
	void Update () {

        Direction updir = (Direction)playerController.getUpDir();
        float percentToRight = 0f;
        Vector3 camTarget = Vector3.zero;

        switch (updir)
        {
            case Direction.y:
                // Must check right dir
                percentToRight = playerCharacter.transform.localPosition.x / turnpoint;
                camTarget = transform.rotation.eulerAngles;
                camTarget.y = -45 / 2 * percentToRight;

                transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(camTarget), rotationSpeed * Time.deltaTime);

                break;

            case Direction.negy:
                break;

            case Direction.x:

                break;

            case Direction.negx:
                break;

            case Direction.z:
                break;

            case Direction.negz:
                break;


            default:
                Debug.Log("Unknown dir");
                break;
        }
    }
}

    /*
	// Use this for initialization
	void Start () {
	    // Set initial rotation
        transform.rotation = Quaternion.LookRotation(-playerCharacter.transform.position);

	}
	
	// Update is called once per frame
	void Update () {
	    // Set rotation based on player position 
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-playerCharacter.transform.position), Time.deltaTime * rotationSpeed);
	} */
