﻿using UnityEngine;
using System.Collections;

public class BeastScript : MonoBehaviour {
    public float minWaitTime = 0f;
    public float maxWaitTime = 15f;
    public float minStareTime = 3f;
    public float maxStareTime = 10f;
    public AudioClip monsterMoan;
    public AudioClip scream;

    private AudioSource beastMoaner;

    public float minTimeVisible = 0.0f;
    public float maxTimeVisible = 0.05f;

    public float minTimeHidden = 0.0f;
    public float maxTimeHidden = 0.8f;


    private Light light;
    private GameObject beastGraphics;
    private bool waiting = true;
    private bool moveToStare = false;
    private bool staring = false;
    private bool attack = false;
    private float waitTime = 0f;
    private float waitDoneTime = 0f;

    private float stareTime = 0f;
    private float stareDoneTime = 0f;

    private float timeTriggered = 0f;
    private float timeStartedStaring = 0f;
    private Transform starePoint;
    private Transform cameraFacePoint;

    private Vector3 startPos;
    private Quaternion startRot;
    private float lastStep = 0f;
    private float originalVolume;
    private bool stopFlicker = false;

	// Use this for initialization
	void Start () {
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        beastMoaner = GameObject.FindGameObjectWithTag("BeastMoaner").GetComponent<AudioSource>();
        originalVolume = beastMoaner.volume;
        waitTime = Random.Range(minWaitTime, maxWaitTime);
        timeTriggered = Time.time;
        cameraFacePoint = Camera.main.transform.FindChild("BeastEndPoint");

        beastGraphics = transform.FindChild("Graphics").gameObject;
        if (beastGraphics)
        {
            StartCoroutine(Flick());
        }

        Debug.Log("Beast waiting for " + waitTime + " seconds");
	}
	
	// Update is called once per frame
	void Update () {
        if (starePoint == null)
        {
            return;
        }
        //Remove beast when light is turned on
        if (Input.GetKeyDown(KeyCode.F) || light.enabled) {
            Debug.Log("Killing beast");
            beastMoaner.Stop();
            beastMoaner.volume = originalVolume;
            Destroy(gameObject);
        }

        //Wait before move to stare point
        if (waiting)
        {
            if (Time.time >= timeTriggered + waitTime)
            {
                Debug.Log("Beast done waiting");
                waiting = false;
                moveToStare = true;
                waitDoneTime = Time.time;
                lastStep = 0f;
                transform.parent = starePoint;

                startPos = transform.localPosition;
                startRot = transform.rotation;
                beastMoaner.PlayOneShot(monsterMoan);
                
            }
        }

        //Move to stare point
        if (moveToStare)
        {
            if (transform.localPosition != starePoint.localPosition)
            {
                float t = (Time.time - waitDoneTime) * 0.2f;
                t = t * t * t * (t * (6f * t - 15f) + 10f);
                if (lastStep > t)
                {
                    t = 1f;
                }
                lastStep = t;

                transform.localPosition = Vector3.Lerp(startPos, starePoint.localPosition, t);
                transform.rotation = Quaternion.Lerp(startRot, starePoint.rotation, t);
            }
            else
            {
                moveToStare = false;
                staring = true;
                timeStartedStaring = Time.time;
                stareTime = Random.Range(minStareTime, maxStareTime);
                Debug.Log("Beast staring for " + stareTime + " seconds");
            }
        }

        //Stare for a while before moving to kill camera
        if (staring)
        {
            if (Time.time >= timeStartedStaring + stareTime)
            {
                Debug.Log("Beast done staring");
                staring = false;
                attack = true;
                stareDoneTime = Time.time;
                lastStep = 0f;
                transform.parent = Camera.main.transform;
                stopFlicker = true;


                startPos = transform.localPosition;
                startRot = transform.rotation;
                beastMoaner.Stop();
                beastMoaner.volume = 1.5f;
                //beastMoaner.pitch = 0.65f;
                beastMoaner.PlayOneShot(scream);
                
            }
        }

        //Move to attack camera 
        if (attack)
        {
            if (transform.localPosition != cameraFacePoint.localPosition)
            {
                float t = (Time.time - stareDoneTime) * 5f;
                t = t * t * t * (t * (6f * t - 15f) + 10f);
                if (lastStep > t)
                {
                    t = 1f;
                }
                lastStep = t;

                transform.localPosition = Vector3.Lerp(startPos, cameraFacePoint.localPosition, t);
                transform.rotation = Quaternion.Lerp(startRot, cameraFacePoint.rotation, t);
            }
            else
            {
                GameLost();
                attack = false;
            }
        }

        
	}

    public void SetStarePoint(Transform strPoint)
    {
        starePoint = strPoint;
    }

    void GameLost()
    {
        Debug.Log("Lost");
        GameObject.FindGameObjectWithTag("GameMaster").GetComponent<ScreenFader>().sceneEnding = true;
    }

    IEnumerator Flick()
    {
        while (!stopFlicker)
        {
            if (beastGraphics.activeSelf)
            {
                yield return new WaitForSeconds(Random.Range(minTimeVisible, maxTimeVisible));
                
            }
            else
            {
                yield return new WaitForSeconds(Random.Range(minTimeHidden, maxTimeHidden));
            }
            beastGraphics.SetActive(!beastGraphics.activeSelf);
        }
        beastGraphics.SetActive(true);
    }
}
