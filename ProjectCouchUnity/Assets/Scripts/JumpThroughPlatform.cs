﻿using UnityEngine;
using System.Collections;

public class JumpThroughPlatform : MonoBehaviour {

    private GameObject playerObject;
    private Rigidbody playerRB;
    private Controller playerController;
    private bool headFirst = false;
    enum Direction { x, y, z, negx, negy, negz };

	// Use this for initialization
	void Start () {
        playerObject = GameObject.FindGameObjectWithTag("Player");

        playerRB = playerObject.GetComponent<Rigidbody>();
        playerController = playerObject.GetComponent<Controller>();
	}


    void OnTriggerEnter(Collider other)
    {

        if (other.tag == "HeadCollider")
        {
            Direction updir = (Direction)playerController.getUpDir();

            switch (updir)
            {
                case Direction.y:
                    if (playerRB.velocity.y > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }

                    break;

                case Direction.negy:
                    if (playerRB.velocity.y < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }
                    break;

                case Direction.x:
                    if (playerRB.velocity.x > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }

                    break;

                case Direction.negx:
                    if (playerRB.velocity.x < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }
                    break;

                case Direction.z:
                    if (playerRB.velocity.z > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }

                    break;

                case Direction.negz:
                    if (playerRB.velocity.z < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                        headFirst = true;
                    }
                    break;


                default:
                    Debug.Log("Unknown dir");
                    break;
            }
            
        }

        else if (headFirst && other.tag == "Player")
        {
            Direction updir = (Direction)playerController.getUpDir();

            switch (updir)
            {
                case Direction.y:
                    if (playerRB.velocity.y > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }

                    break;

                case Direction.negy:
                    if (playerRB.velocity.y < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }
                    break;

                case Direction.x:
                    if (playerRB.velocity.x > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }

                    break;

                case Direction.negx:
                    if (playerRB.velocity.x < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }
                    break;

                case Direction.z:
                    if (playerRB.velocity.z > 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }

                    break;

                case Direction.negz:
                    if (playerRB.velocity.z < 0)
                    {
                        Physics.IgnoreCollision(transform.parent.collider, other);
                    }
                    break;


                default:
                    Debug.Log("Unknown dir");
                    break;
            }
        }
    }


    void OnTriggerExit(Collider other)
    {

        if (other.tag == "Player")
        {
            Physics.IgnoreCollision(transform.parent.collider, other, false);
            headFirst = false;
        }
    }
}
