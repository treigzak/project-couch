﻿using UnityEngine;
using System.Collections;

public class DisabledWhenLightsOff : MonoBehaviour {

    private Light light;
    private MeshRenderer meshRenderer;
    private BoxCollider boxCollider;

    // Use this for initialization
    void Start()
    {
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        meshRenderer = GetComponent<MeshRenderer>();
        boxCollider = GetComponent<BoxCollider>();

        meshRenderer.enabled = light.enabled;
        boxCollider.enabled = light.enabled;
    }

    // Update is called once per frame
    void Update()
    {
        if (meshRenderer.enabled != light.enabled)
        {
            meshRenderer.enabled = light.enabled;
            boxCollider.enabled = light.enabled;
            foreach (Transform child in transform)
            {
                if (child.tag == "Grabbable")
                {
                    child.gameObject.SetActive(light.enabled);
                }
            }
        }

    }
}
