﻿using UnityEngine;
using System.Collections;

public class GrammophoneHandler : MonoBehaviour {

    public AudioClip[] songs;


    int currentSong = 0;
    AudioSource grammophoneSource;
    Light light;
    bool lightOn;

	// Use this for initialization
	void Start () {
        grammophoneSource = GetComponent<AudioSource>();
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        lightOn = light.enabled;

        // Start on a random song
        if (songs.Length > 0)
        {
            currentSong = Random.Range(0, songs.Length - 1);
        }
        
	}
	
	// Update is called once per frame
	void Update () {

        if (GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>().inPreGameState == true)
        {

            return;

        }

        if (songs.Length == 0)
        {
            return;
        }
        if (lightOn != light.enabled)
        {
            lightOn = light.enabled;
            if (!grammophoneSource.isPlaying)
            {
                grammophoneSource.Play();
            }
            else
            {
                grammophoneSource.Pause();
            }
            
            Debug.Log("Pause/unpause song");
        }

        if (grammophoneSource.isPlaying || !lightOn)
        {
            return;
        }

        
        Debug.Log("Starting new song");
        currentSong++;

        if (currentSong >= songs.Length)
        {
            currentSong = 0;
        }
        
        grammophoneSource.clip = songs[currentSong];
        grammophoneSource.Play();
	}
}
