﻿using UnityEngine;
using System.Collections;

public class EnemyStopToShootController : MonoBehaviour {

    [Header("Movement")]
    public float walkSpeed = 100f;

    [Header("Attacking")]
    public GameObject bulletPrefab;
    public float fireRate = 0.5f;
    public float projectileSpeed = 2f;
    public float projectileLocalscale = 0.01750255f;

    private float nextFire = 0.0f;
    private bool attacking;
    private Transform characterGraphics;
    private Transform gunTransform;
    private bool goRight = true;
    private Rigidbody myRB;
    //private float enemyScaleX;
    private bool enabled;

	// Use this for initialization
	void Start () {
        enabled = false;
        characterGraphics = transform.FindChild("Character Graphics");
        //enemyScaleX = characterGraphics.localScale.x;
        gunTransform = characterGraphics.FindChild("Gun");
        myRB = GetComponent<Rigidbody>();

        if (characterGraphics.localScale.x > 0) {
            goRight = true;
        }
        else
        {
            goRight = false;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (!enabled)
        {
            return;
        }
        // If enemy state is attacking
        if (attacking)
        {
            myRB.velocity = Vector3.zero;
            if (Time.time >= nextFire)
            {
                nextFire = Time.time + fireRate;
                GameObject newBullet = (GameObject)Instantiate(bulletPrefab, gunTransform.position, gunTransform.rotation);

                // Since the enemy resides under rigidbodies, parent bullets to that cubes rigid bodies as well to freeze upon transition
                newBullet.transform.parent = transform.parent; 

                newBullet.transform.localScale = new Vector3(projectileLocalscale, projectileLocalscale, projectileLocalscale);

                Vector3 shotVelocityNormalized = Vector3.zero;

                // If the enemy is looking right, shoot right
                if (characterGraphics.localScale.x > 0)
                {
                    shotVelocityNormalized = gunTransform.forward * projectileSpeed;
                }
                // If the enemy is looking left, shoot left
                else
                {
                    shotVelocityNormalized = -gunTransform.forward * projectileSpeed;
                }

                newBullet.rigidbody.velocity = shotVelocityNormalized;


            }
            return;

        }


        // Else patrol
        Vector3 targetVelocity;

        if (goRight)
        {
            targetVelocity = transform.right;
        }
        else
        {
            targetVelocity = -transform.right;
        }


        //targetVelocity = transform.TransformDirection(targetVelocity.normalized) * walkSpeed * Time.deltaTime;
        
        targetVelocity = targetVelocity * walkSpeed * Time.deltaTime;
        Vector3 velocityChange = (targetVelocity - myRB.velocity);


        myRB.AddForce(velocityChange, ForceMode.VelocityChange);

	}

    public void SetShootingState(bool attacking)
    {
        this.attacking = attacking;
        Debug.Log("Change state");
    }

    public void SetEnabled(bool enabled)
    {
        this.enabled = enabled;
    }

    public void TurnAround()
    {
        goRight = !goRight;
        characterGraphics.localScale = new Vector3(characterGraphics.localScale.x * -1, characterGraphics.localScale.y, characterGraphics.localScale.z);
    }
}
