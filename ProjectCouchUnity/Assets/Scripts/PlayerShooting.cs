﻿using UnityEngine;
using System.Collections;

public class PlayerShooting : MonoBehaviour {


    public GameObject bulletPrefab;
    public float fireRate = 0.5f;
    public float projectileSpeed = 2f;
    public float projectileLocalscale = 0.01750255f;
    public AudioClip attackSound;

    private float nextFire = 0.0f;
    private Transform playerObject;
    private Controller playerController;
    private Vector3 lastShotVector = Vector3.zero;
    private AudioSource musicPlayer;

    

    void Start()
    {
        musicPlayer = GameObject.FindGameObjectWithTag("MusicAtCamera").GetComponent<AudioSource>();
        playerObject = transform.parent.parent;
        playerController = playerObject.GetComponent<Controller>();
    }
	
	void Update () {
        
        if (!playerController.controllerAble)
        {
            return;
        }

        if (Input.GetButton("Fire1") && Time.time >= nextFire)
        {
            nextFire = Time.time + fireRate;
            GameObject newBullet = (GameObject)Instantiate(bulletPrefab, transform.position, transform.rotation);
            
            // Since the enemy resides under rigidbodies, parent bullets to that cubes rigid bodies as well to freeze upon transition
            newBullet.transform.parent = playerObject.parent.FindChild("RigidBodies");
            //newBullet.transform.parent = playerObject.transform.parent;
            newBullet.transform.localScale = new Vector3(projectileLocalscale, projectileLocalscale, projectileLocalscale);

            Vector3 shotVelocityNormalized;

            // If player is not pressing any axis, default to shooting forward
            if (Input.GetAxisRaw("Horizontal") == 0 && Input.GetAxisRaw("Vertical") == 0)
            {
                shotVelocityNormalized = transform.forward;

            }
            // Else if the player is pushing in any axis, use that
            else
            {

                float horizontal = Mathf.Abs(Input.GetAxisRaw("Horizontal"));
                
                shotVelocityNormalized = Vector3.Normalize(transform.forward * horizontal + transform.up * Input.GetAxisRaw("Vertical"));
            }

            lastShotVector = shotVelocityNormalized * projectileSpeed;
            newBullet.rigidbody.velocity = lastShotVector;

            musicPlayer.PlayOneShot(attackSound);

            return;
            
        }
	}

    public void ResetShootDir()
    {
        lastShotVector = Vector3.zero;
    }
}