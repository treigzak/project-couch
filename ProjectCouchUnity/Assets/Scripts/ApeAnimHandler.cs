﻿using UnityEngine;
using System.Collections;

public class ApeAnimHandler : MonoBehaviour {

    Animator animator;
    Controller playerController;
    int jumpHash = Animator.StringToHash("Jump");
    int landHash = Animator.StringToHash("Land");
    //int walkHash = Animator.StringToHash("Base Layer.Walk");
    //int idleHash = Animator.StringToHash("Base Layer.Idle");

	// Use this for initialization
	void Start () {
        animator = transform.GetComponent<Animator>();
        playerController = transform.parent.parent.GetComponent<Controller>();
        
	}
	
	// Update is called once per frame
	void Update () {
        //  If player is not grounded, play fall anim
        if (!playerController.GetGrounded() && !animator.GetBool("Falling"))
        {
            animator.SetBool("Falling", true);
        }
        else if (playerController.GetGrounded() && animator.GetBool("Falling"))
        {
            animator.SetBool("Falling", false);
            animator.SetTrigger(landHash);
        }


        if (Input.GetAxisRaw("Horizontal") != 0 && !animator.GetBool("Walking"))
        {
            animator.SetBool("Walking", true);
        }

        else if (Input.GetAxisRaw("Horizontal") == 0 && animator.GetBool("Walking"))
        {
            animator.SetBool("Walking", false);
        }


        //AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);

        //// If jump button is pressed and char can jump, then play jump anim.
        //if (Input.GetButtonDown("Jump") && playerController.jumpsDone < playerController.maxJumps - 1)
        //{
        //    animator.SetTrigger(jumpHash);
        //}
	}

    // Is called from the controller when player jumps
    public void PlayJumpAnim() {
        animator.SetTrigger(jumpHash);
    }
}
