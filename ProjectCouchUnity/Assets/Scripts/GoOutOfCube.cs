﻿using UnityEngine;
using System.Collections;

public class GoOutOfCube : MonoBehaviour {

    public int thisId = 0;
    public int connectedToId = 0;
    GameMaster gm;
    

    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("GameMaster").GetComponent<GameMaster>();
    }

    void OnTriggerStay(Collider what)
    {
        // If player is in trigger zone and pushes down and is not shooting
        if (what.gameObject.tag == "Player" && Input.GetAxisRaw("Vertical") < 0 && !Input.GetButton("Fire1") && Mathf.Abs(Input.GetAxisRaw("Horizontal")) == 0f)
        {
            gm.GoOutwardOneLevel(connectedToId);
        }

    }
}
