﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextChangeWithLight : MonoBehaviour {

    private string lightOnText = "";
    public string lightOffText = "";

    private Light light;
    private Text text;
    private bool displayLightText = true;

	// Use this for initialization
	void Start () {

        

        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        text = GetComponent<Text>();

        lightOnText = text.text;

        text.text = (light.enabled) ? lightOnText : lightOffText;
        displayLightText = light.enabled;
	}
	
	// Update is called once per frame
	void Update () {
        if (light.enabled != displayLightText)
        {
            text.text = (light.enabled) ? lightOnText : lightOffText;
            displayLightText = light.enabled;
        }
	}
}
