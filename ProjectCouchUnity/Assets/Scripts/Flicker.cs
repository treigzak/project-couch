﻿using UnityEngine;
using System.Collections;

public class Flicker : MonoBehaviour {

    public Light holoLight;

    private Color color;
    private float originalLightIntensity;

	// Use this for initialization
	void Start () {
        color = renderer.material.GetColor("_TintColor");
        if (holoLight)
        {
            originalLightIntensity = holoLight.intensity / 2;
        }

        StartCoroutine(Flick());
	}

    IEnumerator Flick()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.0f, 0.1f));
            float random = Random.Range(0.35f, 0.55f);
            color.a = random;
            if (holoLight)
            {
                holoLight.intensity = originalLightIntensity + random * 1.5f;
            }
            renderer.material.SetColor("_TintColor", color);
        }
    }
}
