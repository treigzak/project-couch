﻿using UnityEngine;
using System.Collections;

public class ProjectileLogic : MonoBehaviour {

    public int projectileDamage = 50;


    void OnCollisionEnter(Collision what)
    {
        if (what.gameObject.tag == "Enemy" || what.gameObject.tag == "Player")
        {
            // Call enemy script to take damage
            what.transform.GetComponent<Health>().TakeDamage(projectileDamage);
        }

        Destroy(gameObject);
    }
}
