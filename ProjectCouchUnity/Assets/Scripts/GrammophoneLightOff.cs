﻿using UnityEngine;
using System.Collections;

public class GrammophoneLightOff : MonoBehaviour {

    public AudioClip songToPlay;

    AudioSource grammophoneSource;
    Light light;
    bool lightOn;

	// Use this for initialization
	void Start () {
        grammophoneSource = GetComponent<AudioSource>();
        light = GameObject.FindGameObjectWithTag("MasterLight").GetComponent<Light>();
        lightOn = light.enabled;
        if (songToPlay)
        {
            grammophoneSource.clip = songToPlay;
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        if (!songToPlay)
        {
            return;
        }

        if (lightOn != light.enabled)
        {
            lightOn = light.enabled;
            if (!grammophoneSource.isPlaying)
            {
                grammophoneSource.Play();
            }
            else
            {
                grammophoneSource.Pause();
            }

            Debug.Log("Pause/stop light off song");
        }
	}
}
