﻿using UnityEngine;
using System.Collections;

public class PlatformFlicker : MonoBehaviour {

    private Color color;
    private SetMaterialAlphaBasedOnFace fadeOutScript;

    // Use this for initialization
    void Start()
    {
        color = renderer.material.GetColor("_TintColor");
        fadeOutScript = transform.parent.GetComponent<SetMaterialAlphaBasedOnFace>();

        StartCoroutine(Flick());
    }

    IEnumerator Flick()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(0.0f, 0.1f));

            if (fadeOutScript.canFlick())
            {
                float random = Random.Range(0.35f, 0.55f);
                color.a = random;
                renderer.material.SetColor("_TintColor", color);
            }
            
        }
    }
}
